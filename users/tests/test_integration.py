from rest_framework.test import APITestCase
from users.models import User, UserDiet
from diets.models import Diet

class IntegrationTests(APITestCase):
    def test_complete_flow(self):
        # Registrar usuário
        user = User.objects.create_user(username="testuser", password="password123")

        # Logar
        login_response = self.client.post('/users/login/', {
            "username": "testuser",
            "password": "password123"
        })
        self.assertEqual(login_response.status_code, 200)
        access_token = login_response.data['access']

        # Criar dieta
        diet = Diet.objects.create(name="Test Diet")
        UserDiet.objects.create(user=user, diet=diet, active=True)

        # Buscar dieta
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        diet_response = self.client.get('/users/my_diet/')
        self.assertEqual(diet_response.status_code, 200)
        self.assertEqual(diet_response.data['diet']['name'], "Test Diet")
