import json

from rest_framework.permissions import IsAuthenticated, AllowAny, SAFE_METHODS
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status, viewsets
from django.db.models import Q
from django.utils import timezone 
from rest_framework.pagination import PageNumberPagination

from plates.models import Plate
from diets.models import Diet, DailyHistory, MonthlyHistory
from diets.serializers import DietSerializer
from meals.models import MealPlate
from meals.serializers import MealSerializer
from users.permissions import IsOwnerOrReadOnly
from users.models import UserDiet
from django.db import transaction


class DietPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

class DietViewSet(viewsets.ModelViewSet):
    queryset = Diet.objects.all()
    serializer_class = DietSerializer
    permission_classes=[IsAuthenticated, IsOwnerOrReadOnly]
    pagination_class = DietPagination

    def get_permissions(self):
        """
        Define as permissões com base na ação.
        """
        if self.action in SAFE_METHODS or self.action in ["list", "adopt"]:
            return [AllowAny()]
        
        return super().get_permissions()
    

    def list(self, request):
        filter_param = request.GET.get('filter', None)

        diets = Diet.objects.filter(Q(owner=request.user) | Q(is_active=True) & Q(is_approved=True))

        if filter_param:
            diets = diets.filter(Q(name__icontains=filter_param) | Q(description__icontains=filter_param))

        diets_serialized = DietSerializer(diets, many=True, context={'request': request})
        return Response(diets_serialized.data, status=200)

    
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        user = request.user
        data = request.data

        name = data.get('name')
        description = data.get('description')
        meals_data = data.get('meals')
        image = request.FILES.get('image')

        if not name or not description or not meals_data:
            return Response({'error': 'Nome, descrição, e refeições são requeridos.'}, status=status.HTTP_400_BAD_REQUEST)
        
        if Diet.objects.filter(name=name, owner=user).exists():
            return Response({'error': 'Uma dieta com esse nome já existe.'}, status=status.HTTP_400_BAD_REQUEST)

        meals = []

        if type(meals_data) == str:
            meals_data = json.loads(meals_data)
            
        for meal_data in meals_data:
            meal_serializer = MealSerializer(data=meal_data)
            
            if meal_serializer.is_valid():
                plates_data = meal_data.get('plates', [])
                
                for plate_data in plates_data:
                    plate_id = plate_data.get('plate').get('id')
                    plate = Plate.objects.filter(id=plate_id).first()
                    
                    if not plate_id or not plate:
                        return Response({'error': 'Uma refeição deve ter pelo menos uma prato.'}, status=status.HTTP_400_BAD_REQUEST)
                    
                    meal = meal_serializer.save()
                    
                    meal_plate = MealPlate.objects.create(meal=meal, plate=plate)
                    
                    substitutes_data = plate_data.get('substitutes', [])
                    
                    for substitute_data in substitutes_data:
                        substitute_id = substitute_data.get('id')
                        substitute_plate = Plate.objects.filter(id=substitute_id).first()
                        
                        if not substitute_id or not substitute_plate:
                            meal_plate.delete()
                            meal.delete()
                            return Response({'error': 'Prato substituto inválido.'}, status=status.HTTP_400_BAD_REQUEST)

                        meal_plate.substitutes.add(substitute_plate)
                        
                meals.append(meal)
            else:
                return Response(meal_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        diet = Diet(
            name=name,
            description=description,
            image=image,
            owner=user
        )
        diet.save()
        diet.meals.set(meals)

        diet_serializer = DietSerializer(diet, context={'request': request})
        
        return Response(diet_serializer.data, status=status.HTTP_201_CREATED)
    
    
    @transaction.atomic
    def update(self, request, *args, **kwargs):
        user = request.user
        data = request.data

        diet_id = kwargs.get('pk')
        diet = Diet.objects.filter(id=diet_id, owner=user).first()

        if not diet:
            return Response({'error': 'Dieta não encontrada ou não pertence ao usuário.'}, status=status.HTTP_404_NOT_FOUND)

        name = data.get('name')
        description = data.get('description')
        meals_data = data.get('meals')
        image = request.FILES.get('image')

        if not name or not description or not meals_data:
            return Response({'error': 'Nome, descrição, e refeições são requeridos.'}, status=status.HTTP_400_BAD_REQUEST)

        if Diet.objects.filter(name=name, owner=user).exclude(id=diet.id).exists():
            return Response({'error': 'Uma dieta com esse nome já existe.'}, status=status.HTTP_400_BAD_REQUEST)

        if isinstance(meals_data, str):
            meals_data = json.loads(meals_data)

        # Processar as refeições
        existing_meals = {meal.id: meal for meal in diet.meals.all()}  # Mapear refeições existentes
        updated_meals = []

        for meal_data in meals_data:
            meal_id = meal_data.get('id')
            plates_data = meal_data.get('plates', [])

            if meal_id:  # Atualizar uma refeição existente
                meal = existing_meals.pop(meal_id, None)
                if not meal:
                    return Response({'error': f'Refeição com ID {meal_id} não encontrada.'}, status=status.HTTP_400_BAD_REQUEST)

                meal_serializer = MealSerializer(meal, data=meal_data, partial=True)
                if meal_serializer.is_valid():
                    meal_serializer.save()

                    # Processar pratos na refeição
                    existing_plates = {mp.plate.id: mp for mp in meal.plates.all()}
                    updated_plates = []

                    for plate_data in plates_data:
                        plate_id = plate_data.get('plate', {}).get('id')
                        if not plate_id:
                            return Response({'error': 'Cada prato precisa de um ID.'}, status=status.HTTP_400_BAD_REQUEST)

                        plate = Plate.objects.filter(id=plate_id).first()
                        if not plate:
                            return Response({'error': f'Prato com ID {plate_id} não encontrado.'}, status=status.HTTP_400_BAD_REQUEST)

                        meal_plate = existing_plates.pop(plate_id, None)
                        if not meal_plate:  # Adicionar novo prato à refeição
                            meal_plate = MealPlate.objects.create(meal=meal, plate=plate)

                        # Processar substitutos
                        existing_substitutes = {sub.id for sub in meal_plate.substitutes.all()}
                        updated_substitutes = set()

                        for substitute_data in plate_data.get('substitutes', []):
                            substitute_id = substitute_data.get('id')
                            substitute_plate = Plate.objects.filter(id=substitute_id).first()
                            if not substitute_id or not substitute_plate:
                                return Response({'error': f'Prato substituto inválido: {substitute_id}.'}, status=status.HTTP_400_BAD_REQUEST)

                            updated_substitutes.add(substitute_id)
                            if substitute_id not in existing_substitutes:
                                meal_plate.substitutes.add(substitute_plate)

                        # Remover substitutos antigos
                        for old_substitute_id in existing_substitutes - updated_substitutes:
                            old_substitute = Plate.objects.filter(id=old_substitute_id).first()
                            if old_substitute:
                                meal_plate.substitutes.remove(old_substitute)

                        updated_plates.append(meal_plate)

                    # Remover pratos antigos
                    for old_plate in existing_plates.values():
                        old_plate.delete()

                    updated_meals.append(meal)
                else:
                    return Response(meal_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            else:  # Criar uma nova refeição
                meal_serializer = MealSerializer(data=meal_data)
                if meal_serializer.is_valid():
                    meal = meal_serializer.save()
                    for plate_data in plates_data:
                        plate_id = plate_data.get('plate', {}).get('id')
                        plate = Plate.objects.filter(id=plate_id).first()
                        if not plate:
                            meal.delete()
                            return Response({'error': f'Prato inválido: {plate_id}.'}, status=status.HTTP_400_BAD_REQUEST)

                        meal_plate = MealPlate.objects.create(meal=meal, plate=plate)
                        for substitute_data in plate_data.get('substitutes', []):
                            substitute_id = substitute_data.get('id')
                            substitute_plate = Plate.objects.filter(id=substitute_id).first()
                            if substitute_plate:
                                meal_plate.substitutes.add(substitute_plate)

                    updated_meals.append(meal)
                else:
                    return Response(meal_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # Remover refeições antigas
        for old_meal in existing_meals.values():
            old_meal.delete()

        # Atualizar a dieta
        diet.name = name
        diet.description = description
        diet.image = image if image else diet.image
        diet.save()
        diet.meals.set(updated_meals)

        diet_serializer = DietSerializer(diet, context={'request': request})
        return Response(diet_serializer.data, status=status.HTTP_200_OK)


    
    @transaction.atomic
    @action(detail=True, methods=['post'])
    def adopt(self, request, pk=None):
        diet = self.get_object()
        
        user_diet, created = UserDiet.objects.get_or_create(
            user=request.user,
            diet=diet
        )
        today = timezone.now()
        
        monthly = user_diet.actual_monthly
        daily, created = DailyHistory.objects.get_or_create(
            user=request.user, day=today.day, month=today.month, year=today.year
        )
        
        if daily not in monthly.daily_histories.all():
            monthly.daily_histories.add(daily)
        
        UserDiet.objects.filter(user=request.user).exclude(diet=diet).update(active=False)
        user_diet.active = True
        user_diet.adopted_date = today
        user_diet.save()

        return Response(DietSerializer(diet, context={'request': request}).data, status=status.HTTP_200_OK)
        