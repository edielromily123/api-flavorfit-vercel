import json
import re
import uuid
from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from django.utils.timezone import now
from rest_framework.authentication import TokenAuthentication

from misc.utils import write_log

class RequestLoggingMiddleware(MiddlewareMixin):
    EXCLUDED_PATHS = [
        (settings.FORCE_SCRIPT_NAME + '/misc/requests/record/start/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/misc/requests/record/stop/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/api/users/refresh_data/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/api/token/refresh/', 'POST'),
        (settings.FORCE_SCRIPT_NAME + '/api/ingredient/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/api/plate/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/api/diet/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/test_connection/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/admin/login/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/misc/logs/%s/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/misc/logs/', 'GET'),
        (settings.FORCE_SCRIPT_NAME + '/admin/', 'GET'),
    ]
    
    def is_excluded_path(self, request):
        """
        Valida se o request.path corresponde a algum dos padrões excluídos definidos.
        """
        
        if settings.LOGGING_ENABLED:
            for pattern, method in self.EXCLUDED_PATHS:
                # Substituir o %s no padrão por uma expressão regular para capturar qualquer sequência de caracteres
                if '%s' in pattern:
                    regex_pattern = pattern % r'(.+)'  # Captura tudo após o '/misc/logs/'
                else:
                    regex_pattern = pattern  # Caso não haja '%s', mantém o padrão original
                
                if re.match(regex_pattern, request.path) and request.method == method:
                    return True 
            return False
        return True

    def process_request(self, request):
        if not self.is_excluded_path(request):
            request_id = str(uuid.uuid4())  # Gerando um identificador único para a requisição
            request.META['REQUEST_ID'] = request_id  # Armazenando o request_id no cabeçalho da requisição
            
            user = self._get_authenticated_user(request)
            log_data = {
                "request_id": request_id,
                "timestamp": now().isoformat(),
                "method": request.method,
                "path": request.path,
                "url": request.build_absolute_uri(),
                "user": user.username if user else "Anonymous",
                "headers": dict(request.headers),
            }

            # Verificar e registrar o corpo da requisição
            content_type = request.headers.get('Content-Type', '')
            if 'application/json' in content_type or 'text' in content_type:
                try:
                    log_data["body"] = request.body.decode('utf-8') if request.body else None
                except UnicodeDecodeError:
                    log_data["body"] = f"Error decoding request body (non-UTF-8 content), {request.body}"
            else:
                log_data["body"] = "Non-text body"

            self._write_log(log_data)

    def process_response(self, request, response):
        if not self.is_excluded_path(request):
            request_id = request.META.get('REQUEST_ID', None)  # Obtendo o request_id da requisição
            log_data = {
                "request_id": request_id,
                "timestamp": now().isoformat(),
                "status_code": response.status_code,
                "response_headers": dict(response.items()),
                "response_body": None,
            }

            # Registrar o corpo da resposta se for JSON
            if response.get('Content-Type', '').startswith('application/json'):
                try:
                    log_data["response_body"] = response.content.decode('utf-8')
                except UnicodeDecodeError:
                    log_data["response_body"] = "Error decoding response body"

            self._write_response_log(log_data)

        return response

    def _get_authenticated_user(self, request):
        """
        Recupera o usuário autenticado a partir do token no cabeçalho Authorization.
        """
        auth_header = request.headers.get('Authorization')
        if auth_header and auth_header.startswith('Bearer '):
            token = auth_header.split(' ')[1]
            user_auth = TokenAuthentication()
            try:
                user, _ = user_auth.authenticate_credentials(token.encode())
                return user
            except Exception:
                pass
        return None

    def _write_log(self, log_data):
        """Escreve os logs no arquivo definido."""
        try:
            with open(settings.LOG_RECORD_FILE, "r") as file:
                logs = json.load(file)

                if not isinstance(logs, list):
                    logs = []
        except (FileNotFoundError, json.JSONDecodeError):
            logs = []

        logs.append(log_data)

        with open(settings.LOG_RECORD_FILE, "w") as file:
            json.dump(logs, file, indent=4)
            
    def _write_response_log(self, log_data):
        """Escreve os logs no arquivo definido."""
        try:
            with open(settings.LOG_RECORD_FILE, "r") as file:
                logs = json.load(file)

                if not isinstance(logs, list):
                    logs = []
        except (FileNotFoundError, json.JSONDecodeError):
            logs = []
        
        has_add = False
        for i in range(0, len(logs)):
            if logs[i]['request_id'] == log_data['request_id']:
                logs[i]['response'] = log_data
                has_add = True
                break
        
        if not has_add:
            logs.append(log_data)

        with open(settings.LOG_RECORD_FILE, "w") as file:
            json.dump(logs, file, indent=4)

# Funções para ativar/desativar logging
def is_recording():
    return settings.LOGGING_ENABLED

def start_logging():
    settings.LOGGING_ENABLED = True

def stop_logging():
    settings.LOGGING_ENABLED = False
