from django.contrib import admin
from .models import Diet, DailyHistory, MonthlyHistory, MealHistory

@admin.register(Diet)
class DietAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'owner', 'is_approved', 'is_active')
    list_editable = ('is_approved', 'is_active')
    list_filter = ('is_approved', 'is_active', 'owner')
    search_fields = ('name', 'owner__username')
    list_editable = ('is_approved', 'is_active')
    ordering = ('name', 'id')

@admin.register(DailyHistory)
class DailyHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'day', 'month', 'year', 'weight', 'water_consumed')
    list_filter = ('day', 'month', 'year', 'user')
    search_fields = ('user__username',)
    ordering = ('year', 'month', 'day')
    autocomplete_fields = ('user',)

@admin.register(MonthlyHistory)
class MonthlyHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'month', 'year', 'diet')
    list_filter = ('month', 'year', 'user', 'diet')
    search_fields = ('user__username', 'diet__diet__name')
    ordering = ('year', 'month')
    autocomplete_fields = ('user', 'diet', 'daily_histories')

@admin.register(MealHistory)
class MealHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'daily_history', 'meal', 'is_extra', 'concluded_at')
    list_filter = ('is_extra', 'concluded_at', 'daily_history')
    search_fields = ('meal__name', 'daily_history__user__username')
    ordering = ('-concluded_at',)
