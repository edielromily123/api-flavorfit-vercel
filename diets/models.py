from django.db import models
from meals.managers import DailyHistoryManager
from meals.models import Meal

class Diet(models.Model):
    is_approved = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    name = models.TextField(unique=True)
    image = models.ImageField(upload_to='diets/%Y/%m/%d/', null=True, blank=True)
    description = models.TextField()
    meals = models.ManyToManyField(Meal, related_name='diets')
    owner = models.ForeignKey("users.User", related_name='diet_owner', on_delete=models.CASCADE)
    
    def __str__(self):
        return f"{self.id} - {self.name}"
    
    class Meta:
        unique_together = ('owner', 'name')
        
    @property
    def count_days(self):
        days = [meal.day for meal in self.meals.all()]
        if len(days) > 0:
            return max(days)
        return 0


class DailyHistory(models.Model):
    user = models.ForeignKey("users.User", related_name='daily_history_user', default=None, on_delete=models.CASCADE)
    weight = models.FloatField(null=True, blank=True)
    day = models.IntegerField()
    month = models.IntegerField()
    year = models.IntegerField()
    water_consumed = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    objects = DailyHistoryManager()

    class Meta:
        unique_together = ('user', 'day', 'month', 'year')
        
    def save(self, *args, **kwargs):
        if self.weight is None:  # Define o peso ao criar se não estiver definido
            self.weight = self.user.weight  # Assumindo que o modelo User tem o campo weight
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.day}/{self.month}/{self.year} - {self.water_consumed}L - {self.weight}Kg'
    
    @property
    def meals(self):
        return [meal_history.meal for meal_history in self.meal_histories]
    
    @property
    def meal_histories(self):
        return MealHistory.objects.filter(daily_history=self)
    
    @property
    def monthly_history(self):
        return MonthlyHistory.objects.filter(daily_history=self)
    

class MonthlyHistory(models.Model):
    diet = models.ForeignKey("users.UserDiet", related_name='monthly_history_diet', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey("users.User", related_name='monthly_history_user', default=None, on_delete=models.CASCADE)
    month = models.IntegerField()
    year = models.IntegerField()
    daily_histories = models.ManyToManyField(DailyHistory, related_name='monthly_histories', blank=True)

    class Meta:
        unique_together = ('user', 'month', 'year')

    def __str__(self):
        return f'{self.month}/{self.year}'
    

class MealHistory(models.Model):
    daily_history = models.ForeignKey(DailyHistory, on_delete=models.CASCADE, related_name='meal_entries')
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    is_extra = models.BooleanField(default=False)
    concluded_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f"{self.meal.name} - {self.concluded_at.strftime('%d/%m/%Y')} - {self.is_extra}"