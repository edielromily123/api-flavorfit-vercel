from django.test import TestCase
from users.models import User, UserDiet
from diets.models import Diet
from users.serializers import UserSerializer, UserDietSerializer

class UserSerializerTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser",
            password="password123",
            weight=70,
            height=1.75
        )

    def test_serialize_user(self):
        serializer = UserSerializer(instance=self.user)
        data = serializer.data
        self.assertEqual(data['username'], self.user.username)
        self.assertEqual(data['water_goal'], self.user.water_goal)

    def test_create_user(self):
        data = {
            "username": "newuser",
            "password": "newpassword123",
            "weight": 80
        }
        serializer = UserSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        user = serializer.save()
        self.assertEqual(user.username, "newuser")
        self.assertEqual(user.water_goal, 3)  # 80kg * 35ml = 3 litros


class UserDietSerializerTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="password123")
        self.diet = Diet.objects.create(name="Test Diet")
        self.user_diet = UserDiet.objects.create(user=self.user, diet=self.diet, active=True)

    def test_serialize_user_diet(self):
        serializer = UserDietSerializer(instance=self.user_diet)
        data = serializer.data
        self.assertEqual(data['diet']['name'], "Test Diet")
        self.assertTrue(data['active'])
