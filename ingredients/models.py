from django.db import models

class Ingredient(models.Model):
    is_approved = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    
    amount = 0
    owner = models.ForeignKey("users.User", related_name='ingredient_owner', on_delete=models.CASCADE)
    name = models.TextField()
    calories = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    sodium = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    fibers = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    sugar = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    proteins = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    carbohydrates = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    

    def __str__(self):
        return self.name
    
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'calories', 'sodium', 'fibers', 'sugar', 'proteins', 'carbohydrates', 'owner'],
                name='unique_ingredient'
            )
        ]