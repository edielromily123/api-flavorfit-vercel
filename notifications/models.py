from django.db import models

from notifications.managers import NotificationManager
from .consts import ICONS 

class Notification(models.Model):
    REDIRECTION_TYPES = [
        ('redirect', 'redirect'),
        ('http', 'http'),
    ]
    
    ABLE_TO_REDIRECT_ROUTS = [
        ("/", "/"),
        ("/notifications", "/notifications"),
        ("/calorie_calculator", "/calorie_calculator"),
        ("/auth/complete_register", "/auth/complete_register"),
        ("/login", "/login"),
        ("/user/profile", "/user/profile"),
        ("/user/profile/eidit", "/user/profile/eidit"),
        ("/home", "/home"),
        ("/my_diet", "/my_diet"),
        ("/listing/diets", "/listing/diets"),
        ("/listing/ingredients", "/listing/ingredients"),
        ("/listing/plates", "/listing/plates"),
        ("/register/diet", "/register/diet"),
        ("/register/plate", "/register/plate"),
        ("/select/plate", "/select/plate"),
        ("/chat/support_agent", "/chat/support_agent"),
        ("/test", "/test"),
        ("/plate?plate_id=", "/plate?plate_id="),
        ("/edit/diet?diet_id=", "/edit/diet?diet_id="),
        ("/diet?diet_id=", "/diet?diet_id="),
        ("/edit/plate?plate_id=", "/edit/plate?plate_id="),
        ("/user/profile/other?user_id=", "/user/profile/other?user_id="),
    ]
    
    user = models.ForeignKey("users.User", on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField()
    message = models.TextField()
    title = models.CharField(max_length=255)
    icon = models.CharField(max_length=255, choices=ICONS)  
    created_at = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    is_delivered = models.BooleanField(default=False)
    notification_group = models.CharField(max_length=255, default=None, null=True, blank=True, choices=[('motivation_message', 'Mensagem de motivação'), (None, 'padrão')])
    redirection_type = models.CharField(max_length=255, blank=True, null=True, choices=REDIRECTION_TYPES, help_text="Modifique aqui e salve para mudar o campo de escolhas do redirection ou digitar a url")
    redirection = models.CharField(max_length=255, blank=True, null=True, help_text="O redirection deve ter ou a url com o redirection_type sendo 'http', ou a url da intent caso seja do tipo redirect, para passar parametros em um redirect use o padrão GET de requisições, usando o '?'")
    
    objects = NotificationManager()

    def __str__(self):
        return f'Notification for {self.user.username if self.user else "all users"}: {self.title} - {"delivered" if self.is_delivered else "not delivered"}'

    def mark_as_delivered(self):
        self.is_delivered = True
        self.save()

    def mark_as_read(self):
        self.is_read = True
        self.save()
        
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.is_delivered:
            from users.models import User
            
            if not self.user:
                User.objects.send_notification_to_all(self)
            else:
                User.objects.send_notification_to_user(self.user.id, self)
    
    def to_dict(self):
        return {
            'type': 'send_notification',
            'id': self.id,
            'title': self.title,
            'message': self.message,
            'icon': self.icon,
            'description': self.description,
            'redirection': self.redirection,
            'redirection_type': self.redirection_type,
            'created_at': self.created_at.isoformat(),
            'notification_type': "notification",
        }
        
