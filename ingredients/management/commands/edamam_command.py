import time
import requests
from django.core.management.base import BaseCommand
from ingredients.models import Ingredient
from misc.utils import read_log, write_log

class Command(BaseCommand):
    help = 'Popula o banco de dados com ingredientes da Edamam'

    def handle(self, *args, **kwargs):
        api_key = 'fc225b1b6a6a93714b02e54b6c953467'
        app_id = '65b8f7e0'
        # base_url = f'https://api.edamam.com/api/food-database/v2/parser?app_id={app_id}&app_key={api_key}&ingr='
        base_url = f'https://api.edamam.com/api/food-database/v2/parser?session=CHcDEWgRY0lhWBENQWVcETMBaVU-&app_id=65b8f7e0&app_key=fc225b1b6a6a93714b02e54b6c953467&ingr='

        self.populate_ingredients(base_url)


    def populate_ingredients(self, url):
        # Fazer a requisição para o primeiro ou próximo link de ingredientes
        
        response = requests.get(url)
        data = response.json()
        
        write_log(f'edamam_response__{time.time()}.json', response.json())
        # data = read_log("edamam_response__1732133059.8562865.json")

        for item in data['hints']:
            name = item['food']['label']
            calories = item['food']['nutrients'].get('ENERC_KCAL', None)
            sodium = item['food']['nutrients'].get('NA', None)
            fiber = item['food']['nutrients'].get('FIBTG', None)
            sugar = item['food']['nutrients'].get('SUGAR', None)
            proteins = item['food']['nutrients'].get('PROCNT', None)

            # Traduzir o nome para pt-br
            translated_name = self.translate_to_pt_br(name)

            # Criar o ingrediente no banco
            Ingredient.objects.create(
                name=translated_name,
                calories=calories,
                sodium=sodium,
                fibers=fiber,
                sugar=sugar,
                proteins=proteins,
                is_approved=False,
                is_active=True
            )

        # Verificar se há um link para a próxima página e continuar a requisição
        next_page_url = data.get('_links', {}).get('next', {}).get('href', None)
        if next_page_url:
            print(f"Próxima página: {next_page_url}")
            time.sleep(1)  # Atraso para evitar sobrecarga na API
            self.populate_ingredients(next_page_url)

    def translate_to_pt_br(self, text):
        # Função de tradução
        import googletrans
        translator = googletrans.Translator()
        translation = translator.translate(text, src='en', dest='pt')
        return translation.text
