# Generated by Django 5.0.6 on 2024-11-21 23:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ingredients', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Plate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_approved', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='diets/')),
                ('name', models.TextField(unique=True)),
                ('calories', models.CharField(max_length=10)),
                ('description', models.TextField()),
                ('ingredients', models.ManyToManyField(to='ingredients.ingredient')),
            ],
        ),
    ]
