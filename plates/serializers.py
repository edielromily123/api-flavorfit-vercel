from rest_framework import serializers

from ingredients.serializers import IngredientSerializer
from .models import Plate, Ingredient

class PlateSerializer(serializers.ModelSerializer):
    ingredients = serializers.SerializerMethodField()
    image = serializers.ImageField(max_length=None, allow_empty_file=True, use_url=True)
    owner = serializers.SerializerMethodField()

    class Meta:
        model = Plate
        fields = '__all__'
        
    def get_ingredients(self, instance):
        return IngredientSerializer(instance.ingredients, many=True, context={'plate': instance}).data
        
    def get_owner(self, instance):
        if "request" not in self.context:
            return False
        return instance.owner.id == self.context['request'].user.id