from django.db import models
from colorfield.fields import ColorField

class Card(models.Model):
    GROUPS = [
        ('HOME', 'Home'),
        ('NEW', 'Notícia'),
        ('RECOMENDATION', 'Recomendations'),
        ('REGISTER_DIET_TIP', 'Dicas para criar uma dieta'),
        ('ARTICLE', 'Dicas para criar uma dieta'),
    ]
    
    is_active = models.BooleanField(default=True)
    is_approved = models.BooleanField(default=False)
    group = models.CharField(max_length=255)
    name = models.TextField(default="")
    description = models.TextField(default="")
    image = models.ImageField(upload_to='misc/cards/%Y/%m/%d/', null=True, blank=True)
    bg_color = ColorField(default='#FF0000', max_length=14)
    redirect_link = models.CharField(max_length=510, default=None, blank=True, null=True)
    
    def __str__(self) -> str:
        return f"{self.name} - {self.group}"
    