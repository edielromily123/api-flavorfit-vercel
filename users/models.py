from datetime import datetime
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken

from dirtyfields import DirtyFieldsMixin

from diets.models import Diet, MonthlyHistory
from users.managers import CustomUserManager, UserManager

class User(DirtyFieldsMixin, AbstractUser):
    image = models.ImageField(upload_to='users/%Y/%m/%d/', null=True, blank=True)
    height = models.FloatField(null=True, blank=True)
    weight = models.FloatField(null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    sex = models.CharField(max_length=2, null=True, blank=True)
    activity_level = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_nutritionist = models.BooleanField(default=False)
    
    objects = CustomUserManager()
    
    @property
    def access_token(self):
        return AccessToken.for_user(self)
    
    def to_dict(self):
        return {
            'pk': self.pk,
            'username': self.username,
            'image': self.image.url if self.image else None,
            'height': self.height,
            'weight': self.weight,
            'age': self.age,
            'sex': self.sex,
            'activity_level': self.activity_level,
            'created_at': self.created_at.strftime('%Y-%m-%d %H:%M:%S')
        }

    def __str__(self):
        return f"{self.pk} - {self.username}"
    
    def get_active_user_diet(self):
        return UserDiet.objects.filter(user=self, active=True).first()
    
    @property
    def water_goal(self):
        if self.weight: 
            goal_in_liters = (self.weight * 35) / 1000  
            return round(goal_in_liters)
        return 0

class UserDiet(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='my_diet')
    diet = models.ForeignKey(Diet, on_delete=models.CASCADE)
    adopted_date = models.DateField(auto_now_add=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.username}'s Diet"
    
    @property
    def actual_monthly(self):
        m = MonthlyHistory.objects.filter(user=self.user, month=timezone.now().month, year=timezone.now().year).first()
        
        if not m:
            m = MonthlyHistory.objects.create(user=self.user, month=timezone.now().month, year=timezone.now().year, diet=self)
        else:
            m.diet = self
            m.save()
        
        return m
    
    @property
    def next_meal(self):
        now = datetime.now()
        current_hour = now.hour
        current_minute = now.minute

        meals = self.diet.meals.all().order_by('day', 'hour', 'minute')

        today_in_minutes = now.day * 1440 + current_hour * 60 + current_minute

        for meal in meals:
            meal_time_in_minutes = meal.day * 1440 + meal.hour * 60 + meal.minute

            if meal_time_in_minutes > today_in_minutes:
                return meal

        return meals.first() if meals.exists() else None
        
