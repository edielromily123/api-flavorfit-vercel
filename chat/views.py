from django.shortcuts import render
from .models import Chat
from .serializers import ChatSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework.decorators import action

class ChatViewSet(viewsets.ModelViewSet):
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    permission_classes=[IsAdminUser]


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    permission_classes=[IsAdminUser]
    
def support(request, chat_id=None):
    chats = Chat.objects.filter(type="support_chat")
    chat = None
    
    if chat_id:
        chat = Chat.objects.filter(id=chat_id).first()
    
    if not chat:
        return render(request, 'chat/chats.html', {"chats": chats})
    
    return render(request, 'chat/chats.html', {"chats": chats, "chat": chat})
