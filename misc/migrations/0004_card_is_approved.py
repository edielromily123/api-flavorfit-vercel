# Generated by Django 5.0.6 on 2024-11-20 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('misc', '0003_card_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='is_approved',
            field=models.BooleanField(default=True),
        ),
    ]
