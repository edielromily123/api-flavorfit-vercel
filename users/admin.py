from django.contrib import admin

from .models import User, UserDiet

@admin.register(User)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', 'height', 'weight', 'age', 'sex', 'activity_level', 'is_nutritionist', 'created_at')
    list_filter = ('is_nutritionist', 'sex', 'activity_level', 'created_at')  # Adiciona filtros para categorias relevantes
    search_fields = ('username', 'email', 'first_name', 'last_name')  # Permite pesquisa por nome de usuário e e-mail
    list_editable = ('is_nutritionist',)  # Permite edição rápida do campo is_nutritionist
    ordering = ('id', 'username')  # Ordena por ID e username
    readonly_fields = ('created_at', 'access_token')  # Campos de somente leitura
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        ('Personal Info', {'fields': ('image', 'height', 'weight', 'age', 'sex', 'activity_level')}),
        ('Permissions', {'fields': ('is_nutritionist', 'is_active', 'is_staff', 'is_superuser')}),
        ('Important Dates', {'fields': ('last_login', 'date_joined', 'created_at')}),
    )
    
@admin.register(UserDiet)
class UserDietAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'diet', 'adopted_date', 'active')  # Exibe campos principais na listagem
    list_filter = ('active', 'adopted_date')  # Filtros laterais para o campo booleano e data
    search_fields = ('user__username', 'diet__name')  # Pesquisa por usuário e dieta
    ordering = ('adopted_date',)  # Ordena pela data de adoção
    autocomplete_fields = ('user', 'diet')  # Habilita autocomplete para os relacionamentos
    
