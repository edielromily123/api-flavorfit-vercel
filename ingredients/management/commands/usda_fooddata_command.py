# usda_fooddata_command.py
import time
import requests
from django.core.management.base import BaseCommand
from ingredients.models import Ingredient
from misc.utils import write_log

class Command(BaseCommand):
    help = 'Popula o banco de dados com ingredientes da USDA FoodData Central'

    def handle(self, *args, **kwargs):
        api_key = 'SUA_API_KEY'
        url = f'https://api.nal.usda.gov/fdc/v1/foods/search?query=apple&api_key={api_key}'

        response = requests.get(url)
        write_log(f'nutritionix_response__{time.time()}.json', response.json().encode())
        
        data = response.json()

        for food in data['foods']:
            name = food['description']
            calories = food.get('foodNutrients', {}).get('energy', {}).get('value', None)
            sodium = food.get('foodNutrients', {}).get('sodium', {}).get('value', None)
            fiber = food.get('foodNutrients', {}).get('fiber', {}).get('value', None)
            sugar = food.get('foodNutrients', {}).get('sugars', {}).get('value', None)
            proteins = food.get('foodNutrients', {}).get('protein', {}).get('value', None)

            # Traduzir o nome para pt-br
            translated_name = self.translate_to_pt_br(name)

            # Criar o ingrediente no banco
            Ingredient.objects.create(
                name=translated_name,
                calories=calories,
                sodium=sodium,
                fibers=fiber,
                sugar=sugar,
                proteins=proteins,
                is_approved=False,
                is_active=True
            )