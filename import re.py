import json

PATTERN_DVIDING_PARMS = "#P#"
PATTERN_DVIDING_FUNCTIONS = "#F#"
FUNCTION_PARAMS_MARKER = "#PMS#"
START_FUNCTION_MARKER = "#SFU#"
END_FUNCTION_MARKER = "#EFU#"

response = f"{START_FUNCTION_MARKER}{PATTERN_DVIDING_FUNCTIONS}create_plate{PATTERN_DVIDING_FUNCTIONS}{FUNCTION_PARAMS_MARKER}{PATTERN_DVIDING_PARMS}" + """{
    "name": "Macarrão à Milanesa",
    "description": "Clássico prato italiano com molho à base de tomate e carne moída, coberto com queijo parmesão ralado.",
    "ingredients": [
        {
            "name": "Macarrão Espaguete",
            "amount": 200,
            "calories": 200,
            "sodium": 2,
            "fibers": 2,
            "sugar": 2,
            "proteins": 7,
            "carbohydrates": 43
        },
        {
            "name": "Carne Moída",
            "amount": 100,
            "calories": 200,
            "sodium": 2,
            "fibers": 0,
            "sugar": 0,
            "proteins": 20,
            "carbohydrates": 0
        }
    ],
    "calories": 700
}""" + f"{PATTERN_DVIDING_PARMS}{FUNCTION_PARAMS_MARKER}{END_FUNCTION_MARKER}"

def parse_command(input_text):
    """
    Parse a command string for function calls and their parameters.

    Args:
        input_text (str): The input string containing the commands.

    Returns:
        list: A list of dictionaries containing function names and parameters.
    """
    # Verifica se as marcações estão presentes e corretamente posicionadas
    if START_FUNCTION_MARKER not in input_text or END_FUNCTION_MARKER not in input_text:
        raise ValueError(f"Input text must contain both '{START_FUNCTION_MARKER}' and '{END_FUNCTION_MARKER}' markers.")

    # Remove as marcações de início e fim
    start_index = input_text.find(START_FUNCTION_MARKER) + len(START_FUNCTION_MARKER)
    end_index = input_text.find(END_FUNCTION_MARKER)

    if start_index == -1 or end_index == -1 or start_index >= end_index:
        raise ValueError(f"Invalid positions for '{START_FUNCTION_MARKER}' and '{END_FUNCTION_MARKER}' in the input text.")

    # Extrai o texto entre as marcações
    input_text = input_text[start_index:end_index].strip()

    # Encontra todas as funções e parâmetros
    functions = []
    while PATTERN_DVIDING_FUNCTIONS in input_text:
        # Encontra o nome da função
        start_fn = input_text.find(PATTERN_DVIDING_FUNCTIONS) + len(PATTERN_DVIDING_FUNCTIONS)
        end_fn = input_text.find(PATTERN_DVIDING_FUNCTIONS, start_fn)
        function_name = input_text[start_fn:end_fn]

        # Remove o nome da função do texto
        input_text = input_text[end_fn + len(PATTERN_DVIDING_FUNCTIONS):]

        # Encontra os parâmetros
        if FUNCTION_PARAMS_MARKER in input_text:
            start_params = input_text.find(FUNCTION_PARAMS_MARKER) + len(FUNCTION_PARAMS_MARKER)
            end_params = input_text.find(FUNCTION_PARAMS_MARKER, start_params)
            params_text = input_text[start_params:end_params].strip()

            # Remove os parâmetros do texto
            input_text = input_text[end_params + len(FUNCTION_PARAMS_MARKER):]

            # Encontra todos os parâmetros com #P#
            param_list = []
            while PATTERN_DVIDING_PARMS in params_text:
                start_param = params_text.find(PATTERN_DVIDING_PARMS) + len(PATTERN_DVIDING_PARMS)
                end_param = params_text.find(PATTERN_DVIDING_PARMS, start_param)
                param = params_text[start_param:end_param].strip()

                try:
                    # Tentativa de parse do JSON
                    param = json.loads(param)
                except json.JSONDecodeError:
                    # Se não for JSON válido, mantém como string
                    pass

                param_list.append(param)
                # Remove o parâmetro processado do texto
                params_text = params_text[end_param + len(PATTERN_DVIDING_PARMS):]

            # Adiciona a função e seus parâmetros à lista
            functions.append({
                "function_name": function_name,
                "parameters": param_list
            })

    return functions

print("Response completa:")
print(response)
print("\n\n")
commands = parse_command(response)
print("Comandos extraídos:")
print(commands)
