from rest_framework.permissions import BasePermission, SAFE_METHODS

from users.models import User

class IsOwnerOrReadOnly(BasePermission):
    """
    Permite acesso apenas ao proprietário do objeto para operações de PUT e DELETE.
    """
    
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        
        if isinstance(obj, User):
            return obj == request.user
        
        return hasattr(obj, 'owner') and obj.owner == request.user
