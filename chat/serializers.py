from decimal import Decimal
from meals.models import Meal
from meals.serializers import MealSerializer
from rest_framework import serializers

from users.models import UserDiet
from users.serializers import UserSerializer
from .models import Chat, Message


class ChatSerializer(serializers.ModelSerializer):
    users = UserSerializer(read_only=True, many=True)

    class Meta:
        model = Chat
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    chat = ChatSerializer(read_only=True)
    sender = UserSerializer(read_only=True)
    
    class Meta:
        model = Message
        fields = '__all__'