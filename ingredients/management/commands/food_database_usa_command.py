from decimal import Decimal, InvalidOperation
import time 
import requests 
from django.core.management.base import BaseCommand 
from ingredients.models import Ingredient 
from misc.utils import translate_to_pt_br, write_log, read_log
import traceback


class Command(BaseCommand):
    request_count = 0
    help = 'Popula o banco de dados com Ingredientes usando a API Open Food Facts'

    def handle(self, *args, **kwargs):
        base_url = "https://api.nal.usda.gov/fdc/v1/foods/search"
        query = ""
        self.populate(base_url, query, 7)

    def populate(self, url, query, page):
        params = {
            'api_key':'1wsHy09Tsqxyes39TwsmUn5gfTHtAkX1P5ImcGc0',
            'query': query,
            'pageSize': 300,
            'pageNumber': page,
            'foodCategory': 'Legumes and Legume Products',
            
            'sortBy': 'publishedDate',
        }
        name = None
        
        file_name = f"food_databse_usa_responses/fruits_and_Fruit_Juices/food_databse_usa_response_page_{page}.json"
        try:
            data = read_log(file_name)
            
            if not data:
                self.request_count += 1
                
                if self.request_count % 10 == 0:
                    time.sleep(600)
                
                self.stdout.write(f"Fazendo requisição")
                response = requests.get(url, params=params, timeout=10)
                
                if response.status_code != 200:
                    self.stdout.write(f"Erro na API Open Food Facts: {response.status_code}")
                    return

                write_log(file_name, response.json())  # Salva o log da resposta
                
                # data = response.json()
            else:
                self.stdout.write(f"Requisição já feita")
                
            products = data.get('foods', [])

            for product in products:
                try:                    
                    name = product.get('commonNames', None)
                    if not name:
                        name = product.get('description', None)
                        if not name:
                            continue
                    nutrients = product.get('foodNutrients', {})
                    calories = None
                    sodium = None
                    fiber = None
                    sugar = None
                    proteins = None

                    # Percorre os nutrientes e extrai os valores
                    for nutrient in nutrients:
                        value = nutrient.get('value', None)
                        nutrientName = nutrient.get('nutrientName')
                        unitName = nutrient.get('unitName')
                        unitName = unitName.upper()
                        
                        if nutrientName == 'Energy':
                            if unitName == 'KCAL':
                                calories = value
                            elif unitName == 'KJ':
                                calories = value / 4.184
                            else:
                                self.stdout.write(f"Medida de energia desconhecida: {unitName}")
                                
                            
                        elif nutrientName == 'Total Sugars':  # Açúcar
                            sugar = value
                        elif nutrientName == 'Sodium, Na':  # Sódio
                            sodium = value
                        elif nutrientName == 'Fiber, total dietary':  # Fibras
                            fiber = value
                        elif nutrientName == 'Protein':  # Proteínas
                            proteins = value

                    try:
                        calories = Decimal(calories) if calories else None
                        sodium = Decimal(sodium) if sodium else None
                        fiber = Decimal(fiber) if fiber else None
                        sugar = Decimal(sugar) if sugar else None
                        proteins = Decimal(proteins) if proteins else None
                    except InvalidOperation as e:
                        print(f"Erro ao converter valores numéricos: {e}")
                        return
                    
                    if len(name.split(' ')) > 2:
                        name = name.split(' ')[0] + ' ' + name.split(' ')[1] 

                    name = translate_to_pt_br(name.replace(',', ''))
                    
                    if (not Ingredient.objects.filter(name=name).exists()) and (not '#' in name.lower()):
                        Ingredient.objects.get_or_create(
                            name=name.capitalize(),
                            defaults={
                                'calories': calories,
                                'sodium': sodium,
                                'fibers': fiber,
                                'sugar': sugar,
                                'proteins': proteins,
                                'is_approved': False,
                                'is_active': True,
                                'owner_id': 15
                            }
                        )
                        
                        self.stdout.write(f"Ingrediente adicionado: {name}")

                except Exception as e:
                    self.stdout.write(f"Falha ao adicionar {name}: {str(e)}")
                    traceback.print_exc()

            
            self.stdout.write(f"Ingredientes adicionados da página {page}")
            # if page * 100 < data.get('count', 0):
            #     self.stdout.write(f"Requisitando página {page + 1}")
                
            self.populate(url, query, page + 1)

        except Exception as e:
            self.stdout.write(f"Erro ao processar Ingredientes: {str(e)}")
            self.populate(url, query, page)
            

