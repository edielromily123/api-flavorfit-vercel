from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from django.db.models import Q


from ingredients.models import Ingredient
from ingredients.serializers import IngredientSerializer


class IngredientPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer
    pagination_class = IngredientPagination
    
    def list(self, request):
        filter_param = request.GET.get('filter', None)
        limit = int(request.GET.get('limit', 10))  # Definindo um valor padrão de 10, se 'limit' não for fornecido
        
        query = """
        SELECT * FROM ingredients_ingredient
        WHERE (owner_id = %s OR is_active = TRUE AND is_approved = TRUE)
        """
        
        params = [request.user.id]
        
        if filter_param:
            query += " AND name LIKE %s"
            params.append(f"%{filter_param}%")
        
        query += " LIMIT %s"
        params.append(limit)
        
        ingredients = Ingredient.objects.raw(query, params)
        
        ingredient_serialized = IngredientSerializer(ingredients, many=True, context={'request': request})
        
        return Response(ingredient_serialized.data, status=200)

