from rest_framework.test import APITestCase
from django.urls import reverse
from users.models import User

class UserViewTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="password123")

    def test_login_success(self):
        url = reverse('users-login')
        response = self.client.post(url, {"username": "testuser", "password": "password123"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("access", response.data)

    def test_login_invalid_credentials(self):
        url = reverse('users-login')
        response = self.client.post(url, {"username": "testuser", "password": "wrongpassword"})
        self.assertEqual(response.status_code, 401)

    def test_refresh_data(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('users-refresh-data')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], "testuser")
