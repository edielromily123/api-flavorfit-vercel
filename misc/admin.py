import random
from django import forms
from django.urls import path
from django.contrib import admin
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _

from misc.forms import UpdateFieldForm
from .models import Card


class ActiveFilter(admin.SimpleListFilter):
    title = _('Status')
    parameter_name = 'is_active'

    def lookups(self, request, model_admin):
        return (
            (1, _('Ativos')),
            (0, _('Inativos')),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(is_active=True)
        elif self.value() == '0':
            return queryset.filter(is_active=False)
        return queryset

@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ('name', 'group', 'is_active', 'is_approved', 'bg_color', 'redirect_link')  # Campos a serem exibidos na lista
    list_editable = ('is_active', 'is_approved')
    search_fields = ('name', 'description', 'group')  # Campos pesquisáveis
    ordering = ('group', 'name')  # Ordem de exibição por padrão
    fields = ('name', 'group', 'description', 'image', 'bg_color', 'redirect_link', 'is_active')  # Campos exibidos no formulário de edição
    list_filter = (ActiveFilter, 'group', 'is_active', 'is_approved')
    actions = [
        'duplicate_selected_cards',
        'mark_as_unactive',
        'mark_as_active',
        'mark_as_approved',
        'random_cards',
        'generate_random_bg_color',
        'change_field_in_all',
    ]
    
    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('change_field_in_all/', self.admin_site.admin_view(self.change_field_in_all), name='change_field_in_all'),
        ]
        return custom_urls + urls
    
    def change_field_in_all(self, request, queryset):
        if request.method == 'POST':
            form = UpdateFieldForm(request.POST)
            if form.is_valid():
                selected_field = form.cleaned_data['selected_field']
                new_value = form.cleaned_data['new_value']
                selected_ids = request.POST.getlist('selected_ids')  

                if selected_ids:
                    Card.objects.filter(id__in=selected_ids).update(**{selected_field: new_value})
                    self.message_user(request, "Cartões atualizados com sucesso!")
                    return redirect('admin:misc_card_changelist')
                else:
                    self.message_user(request, "Nenhum cartão selecionado.", level='error')

        else:
            form = UpdateFieldForm()

        context = {
            'form': form,
            'opts': self.model._meta,
            'selected_ids': request.GET.getlist('ids'),  
        }
        return render(request, 'admin/custom_action.html', context)

    def duplicate_selected_cards(self, request, queryset):
        for card in queryset:
            card.pk = None 
            card.save()
        self.message_user(request, "Cartões duplicados com sucesso.")
        
    def mark_as_unactive(self, request, queryset):
        for card in queryset:
            card.is_active = False 
            card.save()
        self.message_user(request, "Cartões marcados como inativos com sucesso.")
    
    def mark_as_active(self, request, queryset):
        for card in queryset:
            card.is_active = True 
            card.save()
        self.message_user(request, "Cartões marcados como ativos com sucesso.")
    
    def mark_as_approved(self, request, queryset):
        for card in queryset:
            card.is_approved = True 
            card.save()
        self.message_user(request, "Cartões aprovados com sucesso.")

    # def temp_action(self, request, queryset):
    #     for card in queryset:
    #         card.name = card.name.capitalize()
    #         card.save()
    #     self.message_user(request, "Ação aplicada com sucesso.")
    
    def random_cards(self, request, queryset):
        grouped_cards = {}
        queryset = Card.objects.filter(group='HOME')
        queryset.update(is_active=False)
        
        for card in queryset:
            if card.group not in grouped_cards:
                grouped_cards[card.group] = []
            grouped_cards[card.group].append(card)

        for group, cards in grouped_cards.items():
            selected_cards = random.sample(cards, min(5, len(cards))) 
            Card.objects.filter(id__in=[card.id for card in selected_cards]).update(is_active=True)

        self.message_user(request, "5 cartões aleatórios de cada grupo foram habilitados.")

    def generate_light_color(self):
        min_value = 128  # Limite mínimo para garantir que a cor seja clara

        r = random.randint(min_value, 255)
        g = random.randint(min_value, 255)
        b = random.randint(min_value, 255)

        # Gerar uma cor em formato 'FFFFRRGGBB'
        return f'FF{r:02x}{g:02x}{b:02x}'
    
    
    def generate_dark_color(self):
        max_value = 128  # Limite mínimo para garantir que a cor seja clara

        r = random.randint(0, max_value)
        g = random.randint(0, max_value)
        b = random.randint(0, max_value)

        # Gerar uma cor em formato 'FFFFRRGGBB'
        return f'FF{r:02x}{g:02x}{b:02x}'

    def generate_random_bg_color(self, request, queryset):
        for card in queryset:
            card.bg_color = self.generate_dark_color()  # Gera uma cor aleatória
            card.save()
        self.message_user(request, "Cores de fundo aleatórias geradas com sucesso.")

    generate_random_bg_color.short_description = "Gerar cor de fundo aleatória"

    random_cards.short_description = "Habilitar apenas 5 cards aleatórios para o home."
    # temp_action.short_description = "Ação temporária"
    
    duplicate_selected_cards.short_description = "Duplicar cartões selecionados"
    mark_as_unactive.short_description = "Marcar cartões como inativos"
    mark_as_active.short_description = "Marcar cartões como ativos"
    mark_as_approved.short_description = "Aprovar cartões"	
    change_field_in_all.short_description = "Mudar um campo em todos"
