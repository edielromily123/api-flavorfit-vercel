from django.test import TestCase
from users.models import User, UserDiet
from diets.models import Diet, MonthlyHistory
from datetime import datetime

class UserModelTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser",
            email="testuser@gmail.com",
            password="password123",
            weight=70,
            height=1.75,
            age=25,
            sex="M",
            activity_level=1.2
        )
        

    def test_create_user(self):
        self.assertEqual(self.user.username, "testuser")
        self.assertEqual(self.user.water_goal, 2)  # 70kg * 35ml = 2 litros

    def test_access_token_generation(self):
        token = self.user.access_token
        self.assertIsNotNone(token)
        self.assertIn("exp", token.payload)

    def test_to_dict(self):
        user_dict = self.user.to_dict()
        self.assertEqual(user_dict['username'], self.user.username)
        self.assertEqual(user_dict['height'], self.user.height)

    def test_get_active_user_diet(self):
        diet = Diet.objects.create(name="Test Diet")
        user_diet = UserDiet.objects.create(user=self.user, diet=diet, active=True)
        self.assertEqual(self.user.get_active_user_diet(), user_diet)


class UserDietTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="password123")
        self.diet = Diet.objects.create(name="Test Diet")
        self.user_diet = UserDiet.objects.create(user=self.user, diet=self.diet, active=True)

    def test_actual_monthly(self):
        monthly_history = self.user_diet.actual_monthly
        self.assertIsInstance(monthly_history, MonthlyHistory)

    def test_next_meal(self):
        self.assertIsNone(self.user_diet.next_meal)  # Nenhuma refeição cadastrada
