import json
import os

from django.http import FileResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q

from rest_framework import viewsets, permissions
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action

from misc.utils import write_log
from users.models import User
from misc.middlewares import logging_middleware
from .models import Card
from .serializers import CardSerializer

tokens = {}

LOGS_DIR = os.path.join(settings.BASE_DIR, 'public', 'static', 'logs')

class CardPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class CardViewSet(viewsets.ModelViewSet):
    queryset = Card.objects.filter(is_active=True, is_approved=True)
    serializer_class = CardSerializer
    pagination_class = CardPagination

    def get_permissions(self):
        if self.request.method in ['GET']:
            return [permissions.AllowAny()]
        return [permissions.IsAdminUser()]

    @action(detail=False, methods=['get'], url_path='group/(?P<group>[^/.]+)')
    def get_by_group_all(self, request, group=None):
        all_cards = request.GET.get('all') == 'true'

        queryset = Card.objects.filter(is_approved=True) if all_cards else self.queryset

        # Aplicar filtro de grupo
        cards = queryset.filter(Q(group__icontains=group.upper()))

        # Filtro adicional opcional (por exemplo, nome ou descrição)
        filter_param = request.GET.get('filter')
        if filter_param:
            cards = cards.filter(Q(name__icontains=filter_param) | Q(description__icontains=filter_param))

        # Paginação
        paginator = self.pagination_class()
        paginated_cards = paginator.paginate_queryset(cards, request)
        serializer = self.get_serializer(paginated_cards, many=True)

        return paginator.get_paginated_response(serializer.data)


def password_reset(request):
    if request.method == 'POST':
        # If the form is for resetting password (Email Form)
        if not request.POST.get('password') and not request.POST.get('confirm_password'):
            email = request.POST.get('email')
            user = User.objects.filter(email=email).first()
            if user:
                token = default_token_generator.make_token(user)
                
                tokens[token] = user.email
                reset_link = f'{request.build_absolute_uri("/misc/auth/forgot-password/")}?token={token}'
                
                send_mail(
                    f'Password Reset - {settings.APP_NAME}',
                    f'Click the link to reset your password: {reset_link}',
                    settings.DEFAULT_FROM_EMAIL,
                    [email]
                )
                return render(request, 'message.html', {'title': 'Confira seu email', 'h1': 'Link de redefinição enviado com sucesso!', 'p': 'Agora você deve checar seu email e clicar no link enviado para redefinir sua senha. Não se esqueça de verificar na caixa de spam.', 'btn_name': 'Sobre nós', 'btn_link': 'about'})
            else:  
                return render(request, 'message.html', {'title': 'Email inválido', 'h1': 'Este email é inválido', 'p': 'Não existe nenhum usuário com este email, volte e verifique novamente.', 'btn_name': 'Mudar senha', 'btn_link': 'password_reset'})

        # If the form is for changing the password (Password Reset Form)
        else:
            password = request.POST.get('password')
            confirm_password = request.POST.get('confirm_password')
            token = request.GET.get('token')
            
            if not token:
                return render(request, 'message.html', {'title': 'Error 400', 'h1': 'Erro com token', 'p': 'Token não fornecido', 'btn_name': 'Mudar senha', 'btn_link': 'password_reset'})
            
            user = User.objects.filter(email=tokens.get(token, None)).first()
            if password == confirm_password and user:
                del tokens[token]
                user.set_password(password)
                user.save()
                return redirect('password_reset_done')
            elif not user:
                return render(request, 'message.html', {'title': 'Redefinição de Senha Falhou', 'h1': 'Sua não senha foi redefinida!', 'p': 'Token inválido ou expirado.', 'btn_name': 'Mudar senha', 'btn_link': 'password_reset'})
            else:
                return render(request, 'message.html', {'title': 'Error 400', 'h1': 'Erro com token', 'p': 'Token não fornecido', 'btn_name': 'Mudar senha', 'btn_link': 'password_reset'})
    token = request.GET.get('token', False)
    
    if token and not tokens.get(token, False):
        return render(request, 'message.html', {'title': 'Error 400', 'h1': 'Erro com token', 'p': 'Token inválido ou expirado', 'btn_name': 'Mudar senha', 'btn_link': 'password_reset'})
    
    return render(request, 'password-reset/password_reset.html', {'reset_password': token})


def password_reset_done(request):
    return render(request, 'message.html', {'title': 'Redefinição de Senha Bem-Sucedida', 'h1': 'Sua senha foi redefinida com sucesso!', 'p': 'Agora você pode fazer login com a sua nova senha.', 'btn_name': 'Sobre nós', 'btn_link': 'about'})

def terms(request):
    return render(request, 'terms.html')

def about(request):
    return render(request, 'about.html')

def download_apk(request):
    apk_path = os.path.join(settings.STATIC_ROOT, 'downloads/DietConecta.apk')
    return FileResponse(open(apk_path, 'rb'), as_attachment=True, filename='DietConecta.apk')

def delete_logs(request, file_name):
    log_file = os.path.join(LOGS_DIR, file_name)

    with open(log_file, 'w') as f:
        f.write('')
        return JsonResponse({}, status=200)
    return JsonResponse({}, status=404)
                   
def logs(request):
    """Exibe a página com a lista de arquivos de log"""
    try:
        log_files = [f for f in os.listdir(LOGS_DIR) if os.path.isfile(os.path.join(LOGS_DIR, f))]
    except FileNotFoundError:
        log_files = []
    return render(request, 'logs_list.html', {'log_files': log_files})


def read_log_file(request, file_name):
    """Lê e processa o conteúdo de um arquivo de log específico"""
    file_path = os.path.join(LOGS_DIR, file_name)

    # Verifica se o arquivo existe
    if not os.path.exists(file_path):
        return JsonResponse({'error': 'Arquivo não encontrado'}, status=404)

    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            all_content = file.read()  # Lê todo o conteúdo do arquivo

        if not all_content:
            return JsonResponse({'error': 'Arquivo vazio'}, status=404)

        # Tenta converter o conteúdo inteiro para JSON
        try:
            log_entry = json.loads(all_content)
            
            if type(log_entry) != list:
                log_entry = [log_entry]
            
            return JsonResponse({'logs': log_entry})
        except json.JSONDecodeError:
            pass  # Se falhar, continuar com a leitura linha por linha

        # Caso a conversão do arquivo inteiro falhe, tenta linha por linha
        logs = []
        for line in all_content.splitlines():
            line = line.strip()
            if line:  # Ignorar linhas vazias
                try:
                    log_entry = json.loads(line)
                    logs.append(log_entry)
                except json.JSONDecodeError:
                    # Caso o JSON seja inválido, adicionar a linha como string
                    logs.append({"raw_entry": line})

        # Se a leitura linha por linha também falhar, retornamos uma resposta com logs brutos
        if not logs:
            return JsonResponse({'error': 'Nenhum log válido encontrado'}, status=404)

        return JsonResponse({'logs': logs})

    except Exception as e:
        # Aqui capturamos qualquer outro erro não esperado
        return JsonResponse({'error': str(e)}, status=500)


def stop_record(request):
    logging_middleware.stop_logging()
    return JsonResponse({}, status=200)

def start_record(request):
    logging_middleware.start_logging()
    return JsonResponse({}, status=200)