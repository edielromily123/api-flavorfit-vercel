from decimal import Decimal
from meals.models import Meal
from meals.serializers import MealSerializer
from rest_framework import serializers

from users.models import UserDiet
from .models import DailyHistory, Diet, MealHistory, MonthlyHistory

class DietSerializer(serializers.ModelSerializer):
    meals = MealSerializer(many=True)
    image = serializers.ImageField(max_length=None, allow_empty_file=True, use_url=True)
    count_days = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()

    class Meta:
        model = Diet
        fields = '__all__'
        
    def get_count_days(self, instance):
        return instance.count_days
    
    def get_owner(self, instance):
        if "request" not in self.context:
            return False
        return instance.owner.id == self.context['request'].user.id


class MealHistorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='meal.id', read_only=True)
    name = serializers.CharField(source='meal.name', read_only=True)
    hour = serializers.IntegerField(source='meal.hour', read_only=True)
    minute = serializers.IntegerField(source='meal.minute', read_only=True)
    day = serializers.IntegerField(source='meal.day', read_only=True)
    description = serializers.CharField(source='meal.description', read_only=True)
    image = serializers.ImageField(source='meal.image', read_only=True)
    concluded_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = MealHistory
        fields = [
            'id', 'name', 'hour', 'minute', 'day', 'description', 'image', 'concluded_at'
        ]
    

class DailyHistorySerializer(serializers.ModelSerializer):
    year = serializers.SerializerMethodField()
    meals = serializers.SerializerMethodField()
    extra_meals = serializers.SerializerMethodField()
    is_complete = serializers.SerializerMethodField()

    class Meta:
        model = DailyHistory
        fields = '__all__'
        
    def get_meals(self, instance):
        return MealHistorySerializer(instance.meal_histories.filter(is_extra=False), many=True).data
    
    def get_extra_meals(self, instance):
        data = MealHistorySerializer(instance.meal_histories.filter(is_extra=True), many=True).data
        return data
    
    def get_is_complete(self, instance):
        user_diet = UserDiet.objects.filter(user=instance.user).first()
        
        if not user_diet:
            return None

        planned_meals = user_diet.diet.meals.all()
        completed_meals = instance.meal_histories.all()

        for meal in planned_meals:
            if not completed_meals.filter(meal=meal).exists():
                return False 
        
        return True 

    
    def get_year(self, instance):
        monthly_history = instance.monthly_histories.first() 
        if monthly_history:
            return monthly_history.year
        return None
        
        
class MonthlyHistorySerializer(serializers.ModelSerializer):
    daily_histories = DailyHistorySerializer(many=True)
    calories = serializers.SerializerMethodField()
    proteins = serializers.SerializerMethodField()
    carbohydrates = serializers.SerializerMethodField()
    sodium = serializers.SerializerMethodField()
    fibers = serializers.SerializerMethodField()
    sugar = serializers.SerializerMethodField()
    consumed_liters_average = serializers.SerializerMethodField()

    class Meta:
        model = MonthlyHistory
        fields = '__all__'
        
    def get_nutrient(self, instance, nutrient):
        total = 0
        for daily_history in instance.daily_histories.all():
            for meal_history in daily_history.meal_histories:
                for meal_plate in meal_history.meal.plates.all():
                    total += sum(
                        ((Decimal(getattr(ingredient, nutrient) or 0) / Decimal(100)) * (ingredient.amount or Decimal(0)))
                        for ingredient in meal_plate.plate.ingredients
                    )
        return total

    def get_calories(self, instance):
        return self.get_nutrient(instance, 'calories')

    def get_proteins(self, instance):
        return self.get_nutrient(instance, 'proteins')

    def get_carbohydrates(self, instance):
        return self.get_nutrient(instance, 'carbohydrates')

    def get_sodium(self, instance):
        return self.get_nutrient(instance, 'sodium')

    def get_fibers(self, instance):
        return self.get_nutrient(instance, 'fibers')

    def get_sugar(self, instance):
        return self.get_nutrient(instance, 'sugar')
    
    def get_consumed_liters_average(self, instance):
        total_liters = 0.0
        total_days = instance.daily_histories.count() 
        
        for daily_history in instance.daily_histories.all():
            water_consumed = float(daily_history.water_consumed)
            water_goal = float(instance.user.water_goal)

            total_liters += water_consumed if water_consumed <= water_goal else water_goal
        
        return total_liters / total_days if total_days > 0 else 0.0

