# Generated by Django 5.0.6 on 2024-11-23 02:03

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ingredients', '0003_remove_ingredient_unique_ingredient_and_more'),
        ('plates', '0002_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plate',
            name='ingredients',
        ),
        migrations.CreateModel(
            name='IngredientPlate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, default=100, max_digits=10)),
                ('ingredient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ingredients.ingredient')),
                ('plate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plates.plate')),
            ],
        ),
    ]
