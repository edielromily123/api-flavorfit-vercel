# admin.py
from django.contrib import admin

from notifications.forms import NotificationAdminForm

from .models import Notification
from django.contrib.auth import get_user_model
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

User = get_user_model()

class NotificationAdmin(admin.ModelAdmin):
    form = NotificationAdminForm
    list_display = ('title', 'message', 'user', 'created_at', 'is_read', 'is_delivered', 'notification_group', 'icon', 'redirection_type', 'redirection')
    list_filter = ('is_read', 'is_delivered', 'created_at')
    search_fields = ('user__username', 'title', 'message')
    list_editable = ('is_read', 'is_delivered')
    actions = ['mark_as_read', 'mark_as_unread', 'mark_as_delivered', 'mark_as_undelivered']

    def send_websocket_notification(self, user, notification):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f"user_{user.id}",
            notification.to_dict()
        )

    def mark_as_read(self, request, queryset):
        queryset.update(is_read=True)

    mark_as_read.short_description = 'Marcar como lidas'

    def mark_as_unread(self, request, queryset):
        queryset.update(is_read=False)

    mark_as_unread.short_description = 'Marcar como não lidas'
    
    def mark_as_delivered(self, request, queryset):
        queryset.update(is_delivered=True)

    mark_as_delivered.short_description = 'Marcar como enviadas'

    def mark_as_undelivered(self, request, queryset):
        for i in queryset:
            i.is_delivered = False
            i.save()

    mark_as_undelivered.short_description = 'Reenviar notificações'

admin.site.register(Notification, NotificationAdmin)
