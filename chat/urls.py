from django.urls import path
from .views import support

urlpatterns = [
    path("support/", support, name="support"),
    path("support/<str:chat_id>/", support, name="support"),
]
