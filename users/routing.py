# routing.py

from django.urls import re_path
from . import consumers
from FlavorFit import settings

websocket_urlpatterns = [
    re_path(rf"{settings.FORCE_SCRIPT_NAME[1::]}/ws/notifications/$", consumers.NotificationConsumer.as_asgi()),
]