import logging
from users.models import User
from .models import Notification

logging.basicConfig(
    filename='../logs/motivation_notifications.log',
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)

logger = logging.getLogger(__name__)

def send_random_motivation_notifications():
    logger.info("Starting to send random motivation notifications...")
    for user in User.objects.all():
        try:
            notification = Notification.objects.get_random_motivation()
            User.objects.send_notification_to_user(user.id, notification)
            logger.info(f"Notification sent to user ID {user.id}")
        except Exception as e:
            logger.error(f"Failed to send notification to user ID {user.id}: {e}")
