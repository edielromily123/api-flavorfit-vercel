### FlavorFit API

**Descrição do Projeto:**
A API do FlavorFit é o backend da aplicação móvel FlavorFit, desenvolvida para gerenciar dietas, monitorar peso e ingestão de água, além de permitir a interação entre usuários e nutricionistas. Esta API foi construída utilizando Django e Django REST Framework, fornecendo endpoints robustos para todas as funcionalidades necessárias.

---

**Pré-requisitos:**
- Python 3.8 ou superior
- Pip (gerenciador de pacotes do Python)

**Instalação:**
1. Clone o repositório:
    ```sh
    git clone https://gitlab.com/my-projects2234415/flavorfit-api.git
    ```
2. Navegue até o diretório do projeto:
    ```sh
    cd flavorfit-api
    ```
3. Instale as dependências a partir do arquivo `requirements.txt`:
    ```sh
    pip install -r requirements.txt
    ```

**Configuração:**
1. Certifique-se de que as configurações do Django estão corretas no arquivo `settings.py`, incluindo configurações de banco de dados, middleware, etc.

**Uso:**
1. Aplique as migrações do banco de dados:
    ```sh
    python manage.py migrate
    ```
2. Inicie o servidor de desenvolvimento:
    ```sh
    python manage.py runserver
    ```
3. A API estará disponível em `http://localhost:8000`, pronta para servir o aplicativo FlavorFit.

---

**Considerações Finais:**
A API do FlavorFit oferece uma infraestrutura robusta e flexível para suportar todas as funcionalidades da aplicação móvel, facilitando o gerenciamento de dietas, monitoramento de saúde e comunicação entre usuários e nutricionistas.

---

**Licença:**
Este projeto está licenciado sob a licença [MIT License](LICENSE).