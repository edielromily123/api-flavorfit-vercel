import json
import logging
import os
from pathlib import Path
from datetime import timedelta
from django.utils.timezone import now
import environ

BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()
environ.Env.read_env(env_file=os.path.join(BASE_DIR, '.env'))

SECRET_KEY = env('SECRET_KEY')
DEBUG = env.bool('DEBUG', default=False)
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=[])

# CORS_ALLOWED_ORIGINS = [
#     'http://localhost:35374',
#     'https://romilydev.online',
# ]

CORS_ALLOW_CREDENTIALS = True

APP_ICON = env('APP_ICON')
APP_NAME = env('APP_NAME')
BOT_NAME = env('BOT_NAME')

CORS_ALLOW_HEADERS = [
    'content-type',
    'authorization',
]

INSTALLED_APPS = [
    'daphne',
    'channels',
    'rest_framework_simplejwt',
    'rest_framework',
    'drf_yasg',
    "admin_interface",
    'corsheaders',
    "colorfield",
    "django_crontab",

    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.auth',
    
    'chat',
    'notifications',
    'ingredients',
    'plates',
    'diets',
    'meals',
    'users',
    'misc',
]

ASGI_APPLICATION = 'FlavorFit.asgi.application'
# WSGI_APPLICATION = 'FlavorFit.wsgi.application'

X_FRAME_OPTIONS = "SAMEORIGIN"
SILENCED_SYSTEM_CHECKS = ["security.W019"]

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [("127.0.0.1", 6379)],
        },
    },
}

SIMPLE_JWT = {
    'SIGNING_KEY': env('JWT_SECRET_KEY'),
    
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=15),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=20),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True,
    'UPDATE_LAST_LOGIN': True,

    'ALGORITHM': 'HS256',
    'VERIFYING_KEY': None,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
}

AUTH_USER_MODEL = 'users.User'

CORS_ALLOW_ALL_ORIGINS = True

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'misc.middlewares.logging_middleware.RequestLoggingMiddleware',
]

ROOT_URLCONF = 'FlavorFit.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'FlavorFit.context_processors.global_variables',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DB_CONNECTION = env('DB_CONNECTION', default='sqlite')  # Valor default para sqlite
DB_HOST = env('DB_HOST', default='127.0.0.1')
DB_PORT = env('DB_PORT', default=3306)
DB_DATABASE = env('DB_DATABASE', default='flavor_fit')
DB_USERNAME = env('DB_USERNAME', default='root')
DB_PASSWORD = env('DB_PASSWORD', default='root')

if DB_CONNECTION == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': DB_DATABASE,
            'USER': DB_USERNAME,
            'PASSWORD': DB_PASSWORD,
            'HOST': DB_HOST,
            'PORT': DB_PORT,
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3',
        }
    }

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

CSRF_TRUSTED_ORIGINS = env('CSRF_TRUSTED_ORIGINS').split(',')

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

DEFAULT_CHARSET = 'utf-8'
USE_UNICODE = True

APPEND_SLASH = True

USE_I18N = True

USE_TZ = False

STATIC_URL = 'static/'

STATIC_ROOT = BASE_DIR / "public/static"
STATICFILES_DIRS = [
    BASE_DIR / "static",
]

FORCE_SCRIPT_NAME = env("FORCE_SCRIPT_NAME")

MEDIA_ROOT = os.path.join(BASE_DIR, 'public/media')
MEDIA_URL = 'media/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

CRONJOBS = [
    (env('CRON_SCHEDULE'), 'notifications.cron.send_random_motivation_notifications')
]

EMAIL_HOST = env('EMAIL_HOST')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_PORT = env.int('EMAIL_PORT', default=587)
EMAIL_USE_TLS = env.bool('EMAIL_USE_TLS', default=True)
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
EMAIL_SENDER_NAME = env('EMAIL_SENDER_NAME')

class JSONFormatter(logging.Formatter):
    def format(self, record):
        # Formata a exceção, se existir
        trace = None
        if record.exc_info:
            trace = self.formatException(record.exc_info)

        log_record = {
            "timestamp": now().isoformat(),
            "level": record.levelname,
            "message": record.getMessage(),
            "logger": record.name,
            "trace": trace,
        }
        return json.dumps(log_record)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'json': {
            '()': JSONFormatter,  # Usa a classe personalizada de formatação JSON
        },
    },
    'handlers': {
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': 'public/static/logs/django_logs.json',
            'formatter': 'json',  # Adiciona o formatter JSON
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

LOGGING_ENABLED = False
LOG_RECORD_FILE = "public/static/logs/request_logs.json"