# admin.py

from django.contrib import admin
from .models import Message, Chat
from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

User = get_user_model()

@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'name', 'created_at')
    search_fields = ('name', 'type')  # Necessário para autocomplete_fields
    autocomplete_fields = ('users',)  # Campo ManyToMany exige isso para melhor performance
    list_filter = ('type', 'created_at')
    ordering = ('-created_at',)
    filter_horizontal = ('users',)

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'content', 'status', 'timestamp', 'sender', 'chat', 'message_hash')
    search_fields = ('content', 'message_hash')  # Necessário para autocomplete_fields
    autocomplete_fields = ('sender', 'chat')  # Relacionamentos
    list_filter = ('status', 'timestamp')
    ordering = ('-timestamp',)
    readonly_fields = ('message_hash',)
    actions = ['mark_as_read', 'mark_as_delivered', 'mark_as_undelivered']

    def send_websocket_message(self, user, message):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f"user_{user.id}",
            message.to_dict()
        )

    def mark_as_read(self, request, queryset):
        queryset.update(status=2)

    mark_as_read.short_description = 'Marcar como lidas'

    def mark_as_delivered(self, request, queryset):
        queryset.update(status=1)

    mark_as_delivered.short_description = 'Marcar como enviadas'

    def mark_as_undelivered(self, request, queryset):
        for i in queryset:
            i.status = 0
            i.save(sender_user=request.user)

    mark_as_undelivered.short_description = 'Reenviar mensagens'

