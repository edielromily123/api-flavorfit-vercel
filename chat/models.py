import uuid

from asgiref.sync import sync_to_async, async_to_sync

from django.db import models
from django.utils import timezone

class Chat(models.Model):
    type = models.CharField(max_length=255)  
    name = models.CharField(max_length=255, null=True, blank=True)  
    users = models.ManyToManyField("users.User", related_name='chats')
    created_at = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.name if self.name else f"Chat {self.id}"
    
    def to_dict(self):
        return {
            "id": self.id,
            "type": self.type,
            "name": self.name,
            "users": [u for u in self.users.values_list("id", flat=True)],
            "created_at": self.created_at.strftime("%d/%m/%Y %H:%M:%S"),
        }
    
    def get_messages(self):
        return Message.objects.filter(chat=self)
    

class Message(models.Model):
    content = models.TextField()
    status = models.IntegerField(default=0)
    timestamp = models.DateTimeField(default=timezone.now)
    sender = models.ForeignKey("users.User", on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='messages')
    message_hash = models.CharField(max_length=255, default=uuid.uuid4, unique=True)
    
    def save(self, sender_user=None,  *args, **kwargs):
        m = Message.objects.filter(message_hash=self.message_hash)
        if m.exists():
            m.first().delete()
        
        super().save(*args, **kwargs)

        if self.status == 0:
            from users.models import User
            
            if sender_user:
                if self.chat.type == "support_chat" and not sender_user.is_superuser:
                    raise ValueError("Apenas usuários admin podem enviar mensagens em support_chat.")
                self.chat.users.add(sender_user)
            
            for user in self.chat.users.exclude(id=self.sender.id):
                async_to_sync(User.objects.send_undelivered_messages)(user.id)

    
    def mark_as_delivered(self):
        self.status = 1
        self.save()
        
    def mark_as_read(self):
        self.status = 2
        self.save()
        
    def to_dict(self):
        return {
            "type": "send_notification",
            "notification_type": "chat_message",
            "id": self.id,
            "content": self.content,
            "status": self.status,
            "timestamp": self.timestamp.strftime('%Y-%m-%d %H:%M:%S'),
            "sender": self.sender.id,
            "chat": self.chat.to_dict(),
            "message_hash": self.message_hash
        }