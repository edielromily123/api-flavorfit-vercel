import json
import os
from django.utils.timezone import now

from FlavorFit import settings

def write_log(file_name, content):
    if not settings.LOGGING_ENABLED:
        return
    
    path = "./public/static/logs"
    
    print(f"{file_name} - {content}")
    
    if not os.path.exists(path):
        os.makedirs(path)
    
    file_path = os.path.join(path, file_name)
    is_json_file = file_name.endswith('.json')
    log_entry = {}

    try:
        log_entry = json.loads(content) if isinstance(content, str) else content
    except (json.JSONDecodeError, TypeError):
        log_entry = {"timestamp": now().isoformat(), "log": str(content)}
    
    if is_json_file:
        try:
            with open(file_path, 'r') as file:
                existing_logs = json.load(file)
                if not isinstance(existing_logs, list):
                    existing_logs = []
        except (FileNotFoundError, json.JSONDecodeError):
            existing_logs = []
        
        existing_logs.append(log_entry)
        
        with open(file_path, 'w') as file:
            json.dump(existing_logs, file, indent=4, ensure_ascii=False)
    else:
        with open(file_path, 'a') as file:
            log_text = f"{now().isoformat()} - {log_entry}\n"
            file.write(log_text)
    
    print(f"Arquivo {file_name} atualizado com sucesso em './logs'.")
    

def read_log(file_name):
    try:
        file_path = os.path.join('./logs', file_name)
        
        with open(file_path, 'r') as file:
            content = file.read()
            
            try:
                content = json.loads(content) 
            except json.JSONDecodeError:
                pass  
        
        return content
    except:
        return None

def translate_to_pt_br(text):
    import googletrans # type: ignore
    translator = googletrans.Translator()
    try:
        translation = translator.translate(text, src='auto', dest='pt')
        return translation.text
    except Exception:
        return text