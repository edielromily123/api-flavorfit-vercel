from django.db import models
from plates.models import Plate

class Meal(models.Model):
    name = models.TextField()
    hour = models.IntegerField()
    minute = models.IntegerField()
    day = models.IntegerField()
    description = models.TextField(blank=True)

    def __str__(self):
        return f"{self.pk} - {self.name}"
    
    @property
    def plates(self):
        return Plate.objects.filter(mealplate__meal=self)
    
    @property
    def diet(self):
        from diets.models import Diet
        
        return Diet.objects.filter(meals=self).first()
    
class MealPlate(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE, related_name='plates')
    plate = models.ForeignKey(Plate, on_delete=models.CASCADE)
    substitutes = models.ManyToManyField(Plate, related_name='substitutes', symmetrical=False)

    def __str__(self):
        return f"Meal {self.meal.id} - Plate {self.plate.id}"