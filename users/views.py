from django.shortcuts import render
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone

from diets.models import DailyHistory, MonthlyHistory
from diets.serializers import DailyHistorySerializer
from meals.models import Meal
from meals.serializers import MealSerializer
from misc.utils import write_log

from .permissions import IsOwnerOrReadOnly
from .models import User, UserDiet
from .serializers import UserSerializer, UserDietSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes=[IsAuthenticated, IsOwnerOrReadOnly]
    extra_kwargs = {'password': {'write_only': True}}

    def get_permissions(self):
        """
        Define as permissões com base na ação.
        """
        if self.action in ['login', 'create', 'test']:
            return [AllowAny()]
        
        return super().get_permissions()


    @action(detail=False, methods=['post'])
    def login(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        
        user = authenticate(request, username=username, password=password)

        if user is None and '@' in username:
            username = User.objects.filter(email=username).first().username
            user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            
            serializer = UserSerializer(user)  # Serializa o usuário autenticado
            refresh = RefreshToken.for_user(user)  # Gera um refresh token
            
            data = serializer.data
            data['refresh'] = str(refresh)
            data['access'] = str(refresh.access_token)
            h, _ = DailyHistory.objects.getActualHistory(request.user)
            
            data['acctual_history'] = DailyHistorySerializer(h).data
            
            return Response(data)
        else:
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
    
    
    @action(detail=False, methods=['get'])
    def refresh_data(self, request):
        user = request.user
        
        if user is not None:
            login(request, user)
            
            serializer = UserSerializer(user)  # Serializa o usuário autenticado
            refresh = RefreshToken.for_user(user)  # Gera um refresh token
            
            data = serializer.data
            data['refresh'] = str(refresh)
            data['access'] = str(refresh.access_token)
            
            user_diet = UserDiet.objects.filter(user=user, active=True).first()
        
            h, has_history = DailyHistory.objects.getActualHistory(request.user)
                
            data['acctual_history'] = DailyHistorySerializer(h).data
            
            # print(str(data).replace("'", '"').replace("True", "true").replace("False", "false").replace("None", "null"))
            
            return Response(data)
        else:
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
    
    
    @action(detail=True, methods=['get'])
    def my_diet(self, request, pk=None):
        diet = UserDiet.objects.get(user=request.user)
        
        if not diet:
            return Response({'error': 'Object Not Found'}, status=status.HTTP_404_NOT_FOUND)
        
        diet_data = UserDietSerializer(diet=diet).data
        
        return Response(diet_data, status=status.HTTP_200_OK)


    @action(detail=False, methods=['post'], permission_classes=[IsAuthenticated])
    def logout(self, request):
        logout(request)
        return Response(status=status.HTTP_200_OK)
    
    
    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def historyes(self, request):
        user = request.user

        if user is not None:
            historyes = DailyHistory.objects.filter(user=user)
            
            return Response(DailyHistorySerializer(historyes, many=True).data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)