import json
from decimal import Decimal

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import status, viewsets
from django.db.models import Q
from django.db import transaction

from ingredients.serializers import IngredientSerializer
from ingredients.models import Ingredient
from misc.utils import write_log
from .serializers import PlateSerializer
from .models import Plate, IngredientPlate
from chat.models import Chat


class PlatePagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class PlateViewSet(viewsets.ModelViewSet):
    queryset = Plate.objects.all()
    serializer_class = PlateSerializer
    pagination_class = PlatePagination

    def list(self, request):
        filter_param = request.GET.get('filter', None)
        excluded_ids = request.GET.get('excluded_ids', None)
        
        if excluded_ids and type(excluded_ids) == str:
            excluded_ids = json.loads(excluded_ids)
        elif not excluded_ids or type(excluded_ids) != list:
            excluded_ids = []
        
        plates = Plate.objects.filter((Q(owner=request.user) | Q(is_active=True, is_approved=True)) & ~Q(id__in=excluded_ids))

        if filter_param:
            plates = plates.filter(Q(name__icontains=filter_param) | Q(description__icontains=filter_param))
        
        write_log('log.json', str([i.id for i in plates]))
        # Aplica paginação
        page = self.paginate_queryset(plates)
        if page is not None:
            serializer = PlateSerializer(page, many=True, context={'request': request})
            
            return self.get_paginated_response(serializer.data)

        plates_serialized = PlateSerializer(plates, many=True, context={'request': request})
        
        
        return Response(plates_serialized.data, status=status.HTTP_200_OK)
    
    
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        user = request.user
        data = request.data

        name = data.get('name')
        description = data.get('description')
        ingredients_data = data.get('ingredients')
        calories_data = data.get('calories')
        chat_id = data.get('chat_id')
        image = request.FILES.get('image')

        # Validação de campos obrigatórios
        if not name or not description or not ingredients_data:
            return Response(
                {'error': 'Nome, descrição, e ingredientes são requeridos.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        # Verifica se um prato com o mesmo nome já existe para o mesmo usuário
        if Plate.objects.filter(name=name, owner=user).exists():
            return Response(
                {'error': 'Um prato com esse nome já existe.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        ingredients = []

        # Se o ingrediente estiver em formato string, converter para JSON
        if isinstance(ingredients_data, str):
            try:
                ingredients_data = json.loads(ingredients_data)
            except json.JSONDecodeError:
                return Response(
                    {'error': 'Formato inválido para ingredientes.'},
                    status=status.HTTP_400_BAD_REQUEST
                )

        # Valida e salva cada ingrediente
        for ingredient_data in ingredients_data:
            ing_id = ingredient_data.get('id')
            ingredient = Ingredient.objects.filter(id=ing_id).first()
            
            if ingredient: 
                change = False
            
                fields_to_update = ['proteins', 'sugar', 'carbohydrates', 'fibers', 'sodium', 'calories']
                
                for field in fields_to_update:
                    if field in ingredient_data and ingredient_data[field] != getattr(ingredient, field, None):
                        setattr(ingredient, field, ingredient_data[field])
                        change = True

                if change:
                    if ingredient.owner != user:
                        ingredient.id = None
                        ingredient.owner = user
                    ingredient.save()
                ingredients.append((ingredient, ingredient_data.get('amount', 100)))
            else:
                ingredient_serializer = IngredientSerializer(data=ingredient_data)
                if ingredient_serializer.is_valid():
                    ingredient = ingredient_serializer.save(owner=user, is_active=False)
                    
                    ingredients.append((ingredient, ingredient_data.get('amount', 100)))
                else:
                    return Response(
                        ingredient_serializer.errors, 
                        status=status.HTTP_400_BAD_REQUEST
                    )

        # Cria o novo prato
        if chat_id:
            chat = Chat.objects.filter(id=chat_id).first()
            u = chat.users.exclude(is_superuser=True).first()
            
            if u:
                user = u
        
        plate = Plate(
            name=name,
            description=description,
            image=image,
            calories=calories_data,
            owner=user,
        )
        plate.save()

        for ingredient in ingredients:
            amount = ingredient[1]
            if isinstance(amount, str) and 'g' in amount:
                try:
                    amount = Decimal(amount.replace('g', '').strip())
                except Exception as e:
                    raise ValueError(f"Erro ao converter quantidade '{amount}' para Decimal: {e}")
            
            IngredientPlate.objects.get_or_create(
                plate=plate,
                ingredient=ingredient[0],
                amount=amount
            )

        # Serializa e retorna o novo prato criado
        plate_serializer = PlateSerializer(plate, context={'request': request})
        return Response(plate_serializer.data, status=status.HTTP_201_CREATED)
    
    
    @transaction.atomic
    def partial_update(self, request, *args, **kwargs):
        plate = self.get_object()
        user = request.user

        data = request.data
        plate.name = data.get('name', plate.name)
        plate.description = data.get('description', plate.description)
        plate.calories = data.get('calories', plate.calories)
        plate.image = request.FILES.get('image', plate.image)
        plate.owner = user

        ingredients_data = data.get('ingredients')
        if ingredients_data:
            if isinstance(ingredients_data, str):
                ingredients_data = json.loads(ingredients_data)

            new_ingredient_ids = []
            fields_to_update = ['proteins', 'sugar', 'carbohydrates', 'fibers', 'sodium', 'calories']

            for ingredient_data in ingredients_data:
                ingredient_id = ingredient_data.get('id')

                if ingredient_id and Ingredient.objects.filter(id=ingredient_id).exists():
                    ingredient = Ingredient.objects.get(id=ingredient_id)
                    change = False

                    for field in fields_to_update:
                        if field in ingredient_data and ingredient_data[field] != getattr(ingredient, field, None):
                            setattr(ingredient, field, ingredient_data[field])
                            change = True

                    if change:
                        if ingredient.owner != user:
                            ingredient.id = None
                            ingredient.owner = user
                        ingredient.save()
                else:
                    ingredient_data['owner'] = user.id
                    ingredient_data['owner_id'] = user.id
                    ingredient_serializer = IngredientSerializer(data=ingredient_data, context={'plate': plate})
                    if ingredient_serializer.is_valid():
                        ingredient = ingredient_serializer.save()
                    else:
                        return Response(ingredient_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

                new_ingredient_ids.append((ingredient, ingredient_data.get('amount', 100)))

            current_ingredients = IngredientPlate.objects.filter(plate=plate).values_list('ingredient_id', flat=True)
            ingredients_to_remove = set(current_ingredients) - set(i[0].id for i in new_ingredient_ids)
            if ingredients_to_remove:
                IngredientPlate.objects.filter(plate=plate, ingredient_id__in=ingredients_to_remove).delete()

            for ingredient, amount in new_ingredient_ids:
                IngredientPlate.objects.update_or_create(plate=plate, ingredient=ingredient, defaults={'amount': amount})

        plate.save()

        plate_serializer = PlateSerializer(plate, context={'request': request})
        return Response(plate_serializer.data, status=status.HTTP_200_OK)
