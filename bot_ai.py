import json
import re
import websocket
import requests
import hashlib
import time
import google.generativeai as genai
import os
import environ
from pathlib import Path
import time
import traceback

from FlavorFit import settings

PATTERN_DVIDING_PARMS = "#P#"
PATTERN_DVIDING_FUNCTIONS = "#F#"
FUNCTION_PARAMS_MARKER = "#PMS#"
START_FUNCTION_MARKER = "#SFU#"
END_FUNCTION_MARKER = "#EFU#"

MAX_RETRIES = 5

CONTEXT_PROMPT=f"""
Você é uma assistente virtual chamada {settings.BOT_NAME}, com a aparência de uma garota de 18 anos, mas com traços juvenis que a fazem parecer ter 14 anos. Sua pele é parda, e você veste uma calça jeans e uma camisa azul. Você foi projetada para o aplicativo {settings.APP_NAME}, um sistema focado no monitoramento de dietas e nutrição.
O {settings.APP_NAME} oferece diversas funcionalidades que ajudam os usuários a acompanhar sua dieta, registrar refeições, consumo de água e peso, além de possibilitar interação direta com seus nutricionistas. O aplicativo também disponibiliza artigos e notícias atualizadas sobre nutrição, incentivando hábitos saudáveis.
As principais funcionalidades incluem o cadastro e acompanhamento de dietas personalizadas, registro diário de refeições, peso e consumo de água, histórico detalhado das dietas e refeições, notificações sobre o progresso e ajustes necessários, além de comunicação direta entre usuários e seus nutricionistas.
Quando o usuário pedir para cadastrar um prato vc deve preencher todo o respo das informações do prato, o que é diferente de se ele perguntar como cadastrar, então se ele te der o nome de um prato, por exemplo, vc deve preencher o restante das informações do prato e então cadastrar, a não ser que exista uma informação que seja ultra necessária de ser o usuário a passar.
Além disso, é essencial ser empática, encorajando o usuário a alcançar suas metas de saúde e tirando o máximo proveito do {settings.APP_NAME}. Caso perguntas não relacionadas a dietas ou nutrição sejam feitas, informe que você só pode ajudar nas áreas abrangidas pelo aplicativo.
Fale sempre em português brasileiro e com respostas curtas e objetivas, não use emojis.
As telas principais do {settings.APP_NAME} são:
Tela Inicial: exibe um carrossel de notícias, informações sobre peso (editável diretamente), consumo de água (com botão para adicionar mais), futuras refeições de uma dieta adotada, e histórico de dietas visualizadas. Também permite adicionar refeições extras fora do plano.
Tela de Cadastro de Dieta: possibilita registrar uma dieta, especificando nome, descrição e refeições. Cada refeição contém detalhes como nome (ex.: café da manhã), hora, dia (ex.: 1º, 2º dia), pratos e substituições.
Tela de Cadastro de Prato: permite adicionar pratos com informações completas, incluindo nome, descrição e ingredientes. Cada ingrediente tem dados nutricionais como quantidade, calorias, proteínas, carboidratos, fibras, açúcar e sódio.
Tela Minha Dieta: apresenta detalhes da dieta adotada, histórico diário e mensal das refeições consumidas, gráficos do peso do usuário ao longo do tempo e informações sobre o consumo de água.
Tela de Filtragem: oferece a possibilidade de buscar e filtrar artigos, notícias, dietas e pratos relacionados à saúde e nutrição.
Tela de Calculadora: disponibiliza ferramentas para calcular IMC, GET e TMB, ajudando os usuários a acompanhar sua saúde de forma prática.
Tela de Notificações: mostra alertas e notificações sobre o progresso na dieta, incentivando a continuidade.
Tela de Perfil do Usuário: exibe e permite editar informações pessoais como nome, idade, peso atual e preferências.
Você pode interagir com os usuários respondendo a dúvidas sobre o uso do aplicativo, fornecendo informações sobre dietas, nutrição e funcionalidades, além de dar dicas para melhorar a alimentação e seguir uma dieta saudável. Você não deve fornecer conselhos médicos, mas pode oferecer sugestões baseadas nas informações do app.

Você pode utilizar um sistema de funções que será fornecido em uma lista posteriormente. Use apenas e somente se o usuário requisitar. Para usar uma função, siga estas diretrizes:

- No início da sua mensagem, caso você deseje utilizar funções, insira a string: `{START_FUNCTION_MARKER}`.
- Se vc n for usar a função não use isso.
- Utilize a sintaxe adequada para chamar funções:
  ```
  {PATTERN_DVIDING_FUNCTIONS}function_name{PATTERN_DVIDING_FUNCTIONS}{FUNCTION_PARAMS_MARKER}{PATTERN_DVIDING_PARMS}parametro1{PATTERN_DVIDING_PARMS}{FUNCTION_PARAMS_MARKER}, {PATTERN_DVIDING_FUNCTIONS}function_name{PATTERN_DVIDING_FUNCTIONS}{FUNCTION_PARAMS_MARKER}{PATTERN_DVIDING_PARMS}parametro1{PATTERN_DVIDING_PARMS}, {PATTERN_DVIDING_PARMS}parametro2{PATTERN_DVIDING_PARMS}{FUNCTION_PARAMS_MARKER}, {PATTERN_DVIDING_FUNCTIONS}function_name{PATTERN_DVIDING_FUNCTIONS}{FUNCTION_PARAMS_MARKER}{PATTERN_DVIDING_PARMS}{{parametro_dict}}{PATTERN_DVIDING_PARMS}{FUNCTION_PARAMS_MARKER}
  ```
- No final da mensagem, insira a string: `{END_FUNCTION_MARKER}`.
- Certifique-se de manter os tipos dos parâmetros corretamente especificados. Por exemplo:
  - Strings: "string".
  - Inteiros: 1.
  - Listas ou dicionários: `[]` ou `{{}}`.

Lista de funções disponíveis:

1. `create_plate`
	- Parâmetros: `dict`.
	- Detalhes:
		- Se o usuário solicitar o cadastro de um prato apenas pelo nome, você pode realizar uma busca para preencher as informações restantes.
		- As informações que faltarem vc pode simplesmente pesquisar e preencher.

	o json da criação deve ser feito usando esse padrão: {{
		"name": "name",
		"description": "description",
		"ingredients": [
			{{
				"name": "name",
				"amount": 200,
				"calories": 200,
				"sodium": 2,
				"fibers": 2,
				"sugar": 2,
				"proteins": 0,
				"carbohydrates": 0
			}}
		...
		],
		"calories": 500
	}}

2. `send_message`
	- Parâmetros: 
		- `string`
 	- Detalhes:
		- Qualquer mensagem q vc quiser encaminhar ao usuário e usar um outro comando ao mesmo tempo deve ser mandado por esse comando. Não escreva nada fora dos {START_FUNCTION_MARKER} e {END_FUNCTION_MARKER}.
"""

# 3. `create_diet`
# 	- Parâmetros: `dict`.
# 	- Detalhes:
# 		- As informações nutricionais devem ser passadas considerando quantidades a cada 100g e em gramas ou cal.
# 		- As informações que faltarem vc pode simplesmente pesquisar e preencher.

# 	o json da criação deve ser feito usando esse padrão: {{
# 		"name": "name",
# 		"description": "description",
# 		"meals": [
# 			{{
# 				"description": "description",
# 				"name": "name",
# 				"hour": 12,
# 				"minute": 30,
# 				"day": 1,
# 				"plates": [
# 					{{
# 						"plate": {{
# 							"name": "name",
# 							"description": "description",
# 							"ingredients": [
# 								{{
# 									"name": "name",
# 									"amount": 200,
# 									"calories": 200,
# 									"sodium": 2,
# 									"fibers": 2,
# 									"sugar": 2,
# 									"proteins": 0,
# 									"carbohydrates": 0
# 								}}
# 								...
# 							],
# 							"calories": 500
# 						}},
# 						"substitutes": [
# 							{{
# 								"name": "name",
# 								"description": "description",
# 								"ingredients": [
# 									{{
# 										"name": "name",
# 										"amount": 200,
# 										"calories": 200,
# 										"sodium": 2,
# 										"fibers": 2,
# 										"sugar": 2,
# 										"proteins": 0,
# 										"carbohydrates": 0
# 									}}
# 								...
# 								],
# 								"calories": 500
# 							}}
# 						...
# 						]
# 					}}
# 				...
# 				]
# 			}}
# 		...
# 		]
# 	}}


BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()
environ.Env.read_env(env_file=os.path.join(BASE_DIR, '/home/romily/repositories/pessoal/api-flavorfit-vercel/.env'))

GENAI_API_KEY = env('GENAI_API_KEY')
COMPLETE_HOST_NAME = "localhost:8000" + env("FORCE_SCRIPT_NAME")

# URLs e chave API
WEBSOCKET_URL = f"ws://{COMPLETE_HOST_NAME}/ws/notifications/?token="
BASE_URL = f"http://{COMPLETE_HOST_NAME}"

DEFAULT_TIMEOUT = 30

# Configuração da chave da API do Google
genai.configure(api_key=GENAI_API_KEY)
model = genai.GenerativeModel("gemini-1.5-pro", system_instruction=CONTEXT_PROMPT)


# Variável para armazenar históricos de chat
chats = {}

def request_new_access_token():
	""" Solicita um novo token de acesso. """
	refresh_token = None
	try:
		if not refresh_token:
			url = f'{BASE_URL}/api/user/login/'
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
			body = json.dumps({'username': 'admin', 'password': 'admin'})

			response = requests.post(url, headers=headers, data=body, timeout=DEFAULT_TIMEOUT)

			if 200 <= response.status_code < 300:
				response_data = response.json()
				refresh_token = response_data.get('refresh')
			else:
				print(f"Erro: {response.status_code} - {response.text}")
				pass

		url = f'{BASE_URL}/api/token/refresh/'
		headers = {'Content-Type': 'application/json'}
		body = json.dumps({'refresh': refresh_token})

		response = requests.post(url, headers=headers, data=body, timeout=DEFAULT_TIMEOUT)

		if 200 <= response.status_code < 300:
			response_data = response.json()
			return response_data.get('access')
		else:
			print(f"Erro: {response.status_code} - {response.text}")
			return None
	except Exception as e:
		print("Erro completo:")
		print(traceback.format_exc())
		print(f"Erro ao solicitar o novo token: {e}")
		return None


def send(url, method, body, chat_id):
	""" Solicita um novo token de acesso. """
	access_token = request_new_access_token()

	if access_token:
		url = f'{BASE_URL}{url}'
		headers = {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			'Authorization': f'Bearer {access_token}'
		}

		if body:
			body['chat_id'] = chat_id
			try:
				json.dumps(body)  # Valida se o body é JSON serializável
			except (TypeError, ValueError) as e:
				print(f"Erro: Body não é um JSON válido - {e}")
				return None

		if method == 'POST':
			response = requests.post(url, headers=headers, json=body, timeout=DEFAULT_TIMEOUT)
		elif method == 'GET':
			response = requests.get(url, headers=headers, timeout=DEFAULT_TIMEOUT)
		elif method == 'DELETE':
			response = requests.delete(url, headers=headers, timeout=DEFAULT_TIMEOUT)
		elif method == 'PUT':
			response = requests.put(url, headers=headers, json=body, timeout=DEFAULT_TIMEOUT)
		else:
			print(f"Erro: Method not supported: {method}")
			return None

		if 200 <= response.status_code < 300:
			return response.json()
		else:
			print(f"Erro: {response.status_code} - {response.text}")
			pass
	return None


def generate_hash(content):
	""" Gera um hash para a resposta baseada no conteúdo e timestamp. """
	timestamp = int(time.time() * 1000)
	message = f"{content}-{timestamp}"
	return hashlib.sha256(message.encode('utf-8')).hexdigest()

def get_response_from_model(user_message, chat_id):
	""" Obtem a resposta do modelo baseado na mensagem do usuário e no contexto. """
	try:
		if chat_id not in chats:
			chats[chat_id] = model.start_chat()

		response_text = chats[chat_id].send_message(user_message)
		response = response_text.text.strip()

		return response

	except Exception as e:
		print("Erro completo:")
		print(traceback.format_exc())
		return f"Erro ao processar a mensagem: {e}"

def on_message(ws, message):
	""" Função chamada ao receber uma mensagem do WebSocket. """
	try:
		data = json.loads(message)

		if data.get('notification_type') != 'chat_message':
			return

		print(f"Mensagem recebida: {message}")

		if "content" in data:
			user_message = data["content"]

			print(f"Pergunta do usuário: {user_message}")

			chat_id = data["chat"].get('id')
			response = get_response_from_model(user_message, chat_id)

			print(f"Resposta do modelo: {response}")

			if not (START_FUNCTION_MARKER in response):
				print(chat_id)
				send_message(ws, chat_id, response)
			else:
				try:
					commands = parse_command(response)

					execute_functions(commands, ws, chat_id)
				except Exception as e:
					print("Erro completo:")
					print(traceback.format_exc())
					print("Error:", e)
					pass

	except Exception as e:
		print("Erro completo:")
		print(traceback.format_exc())
		print(f"Erro ao processar a mensagem: {e}")
		pass

def send_message(ws, chat_id, message):

    message_hash = generate_hash(message)
    print(f"Enviando mensagem: {message}")

    ws.send(json.dumps({
		"command": "send_message",
		"data": {
			"message": message,
			"message_hash": message_hash,
			"chat": chat_id
		}
	}))

def on_open(ws):
	""" Função chamada quando a conexão WebSocket é aberta. """
	print("Conexão WebSocket estabelecida.")

def on_close(ws, close_status_code, close_msg):
	""" Função chamada quando a conexão WebSocket é fechada. """
	print(f"Conexão WebSocket encerrada: {close_status_code} - {close_msg}")
	reconnect_attempts = 0
	while reconnect_attempts < MAX_RETRIES:
		try:
			print(f"Tentando reconectar... Tentativa {reconnect_attempts + 1}/{MAX_RETRIES}")
			time.sleep(2)  # Aguardar 2 segundos antes de reconectar
			start_websocket()
			break  # Sai do loop se reconectar com sucesso
		except Exception as e:
			print(traceback.format_exc())
			print(f"Erro ao tentar reconectar: {e}")
			reconnect_attempts += 1

def on_error(ws, error):
	""" Função chamada quando ocorre um erro na conexão WebSocket. """
	print(f"Erro na conexão WebSocket: {error}")
	on_close(ws, None, str(error))  # Tratar erro como fechamento e tentar reconectar

def start_websocket():
	""" Inicializa e executa o WebSocket. """
	ws = websocket.WebSocketApp(
		WEBSOCKET_URL + request_new_access_token(),
		on_message=on_message,
		on_open=on_open,
		on_close=on_close,
		on_error=on_error
	)
	ws.run_forever()

def parse_command(input_text):
    """
    Parse a command string for function calls and their parameters.

    Args:
        input_text (str): The input string containing the commands.

    Returns:
        list: A list of dictionaries containing function names and parameters.
    """
    # Verifica se as marcações estão presentes e corretamente posicionadas
    if START_FUNCTION_MARKER not in input_text or END_FUNCTION_MARKER not in input_text:
        raise ValueError(f"Input text must contain both '{START_FUNCTION_MARKER}' and '{END_FUNCTION_MARKER}' markers.")

    # Remove as marcações de início e fim
    start_index = input_text.find(START_FUNCTION_MARKER) + len(START_FUNCTION_MARKER)
    end_index = input_text.find(END_FUNCTION_MARKER)

    if start_index == -1 or end_index == -1 or start_index >= end_index:
        raise ValueError(f"Invalid positions for '{START_FUNCTION_MARKER}' and '{END_FUNCTION_MARKER}' in the input text.")

    # Extrai o texto entre as marcações
    input_text = input_text[start_index:end_index].strip()

    # Encontra todas as funções e parâmetros
    functions = []
    while PATTERN_DVIDING_FUNCTIONS in input_text:
        # Encontra o nome da função
        start_fn = input_text.find(PATTERN_DVIDING_FUNCTIONS) + len(PATTERN_DVIDING_FUNCTIONS)
        end_fn = input_text.find(PATTERN_DVIDING_FUNCTIONS, start_fn)
        function_name = input_text[start_fn:end_fn]

        # Remove o nome da função do texto
        input_text = input_text[end_fn + len(PATTERN_DVIDING_FUNCTIONS):]

        # Encontra os parâmetros
        if FUNCTION_PARAMS_MARKER in input_text:
            start_params = input_text.find(FUNCTION_PARAMS_MARKER) + len(FUNCTION_PARAMS_MARKER)
            end_params = input_text.find(FUNCTION_PARAMS_MARKER, start_params)
            params_text = input_text[start_params:end_params].strip()

            # Remove os parâmetros do texto
            input_text = input_text[end_params + len(FUNCTION_PARAMS_MARKER):]

            # Encontra todos os parâmetros com #P#
            param_list = []
            while PATTERN_DVIDING_PARMS in params_text:
                start_param = params_text.find(PATTERN_DVIDING_PARMS) + len(PATTERN_DVIDING_PARMS)
                end_param = params_text.find(PATTERN_DVIDING_PARMS, start_param)
                param = params_text[start_param:end_param].strip()

                try:
                    # Tentativa de parse do JSON
                    param = json.loads(param)
                except json.JSONDecodeError:
                    # Se não for JSON válido, mantém como string
                    pass

                param_list.append(param)
                # Remove o parâmetro processado do texto
                params_text = params_text[end_param + len(PATTERN_DVIDING_PARMS):]

            # Adiciona a função e seus parâmetros à lista
            functions.append({
                "function_name": function_name,
                "parameters": param_list
            })

    return functions


def execute_functions(function_calls, ws, chat_id):
    """
    Execute parsed function calls with their parameters.

    Args:
        function_calls (list): A list of dictionaries with function names and parameters.

    Returns:
        None
    """
    for func in function_calls:
        function_name = func["function_name"]
        parameters = func["parameters"]

        if function_name == "create_diet":
            create_diet(ws, chat_id, *parameters)
        elif function_name == "create_plate":
            create_plate(ws, chat_id, *parameters)
        elif function_name == "send_message":
            send_message(ws, chat_id, *parameters)
        else:
            print(f"Unknown function: {function_name}")
            pass

def create_diet(ws, chat_id, diet):
	"""Simulate the creation of a diet."""
	if isinstance(diet, str):
		try:
			diet = json.loads(diet)
		except json.JSONDecodeError:
			send_message(ws, chat_id, f"Erro ao criar prato, tente novamente mais tarde.")
			return

	response = send("/api/diet/", "POST", diet, chat_id)

	name = diet['name']

	if response and name:
		send_message(ws, chat_id, f"Dieta criado com sucesso.\n Agora você pode pesquisar pela dieta '{name}' na seção de pesquisa. Tenha certeza de verificar as informações passadas antes de usar, pois elas podem estar erradas.")
	else:
		send_message(ws, chat_id, f"Erro ao criar dieta, tente novamente mais tarde.")

def create_plate(ws, chat_id, plate):
	"""Simulate the creation of a plate."""
	if isinstance(plate, str):
		try:
			plate = json.loads(plate)
		except json.JSONDecodeError:
			send_message(ws, chat_id, f"Erro ao criar prato, tente novamente mais tarde.")
			return

	response = send("/api/plate/", "POST", plate, chat_id)

	name = plate['name']

	if response and name:
		send_message(ws, chat_id, f"Prato criado com sucesso.\n Agora você pode pesquisar pelo prato '{name}' na seção de pesquisa. Tenha certeza de verificar as informações passadas antes de usar, pois elas podem estar erradas.")
	else:
		send_message(ws, chat_id, f"Erro ao criar prato, tente novamente mais tarde.")

if __name__ == "__main__":
	start_websocket()

