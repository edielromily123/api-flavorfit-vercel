from rest_framework.permissions import BasePermission, SAFE_METHODS

from users.models import User

class IsOwnerOrReadOnly(BasePermission):
    """
    Permite acesso apenas ao proprietário do objeto para operações de PUT e DELETE.
    """
    
    def has_object_permission(self, request, view, obj):
        # Permite acesso seguro a métodos HTTP GET, HEAD ou OPTIONS.
        if request.method in SAFE_METHODS:
            return True
        
        # Verifica se o objeto é do tipo User.
        if isinstance(obj, User):
            return obj == request.user
        
        # Verifica se o objeto tem um campo owner e se o usuário autenticado é o proprietário.
        return hasattr(obj, 'user') and obj.user == request.user
