from django.contrib import admin
from .models import Plate, IngredientPlate

@admin.register(IngredientPlate)
class IngredientPlateAdmin(admin.ModelAdmin):
    list_display = ('get_plate_name', 'get_ingredient_name', 'amount')  
    list_filter = ('plate__name', 'ingredient__name', 'amount')  
    search_fields = ('plate__name', 'ingredient__name', 'amount')  
    
    def get_plate_name(self, obj):
        return obj.plate.name
    get_plate_name.admin_order_field = 'plate__name'  
    get_plate_name.short_description = 'Plate Name'
    
    def get_ingredient_name(self, obj):
        return obj.ingredient.name
    get_ingredient_name.admin_order_field = 'ingredient__name'  
    get_ingredient_name.short_description = 'Ingredient Name'
    

@admin.register(Plate)
class PlateAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'is_active', 'is_approved', 'calories')  
    list_editable = ('is_approved', 'is_active')
    list_filter = ('is_active', 'is_approved', 'owner')  
    search_fields = ('name', 'description', 'calories')  
    ordering = ('-is_approved', 'name')  
    fields = ('name', 'owner', 'calories', 'description', 'image', 'is_active', 'is_approved')  

    def save_model(self, request, obj, form, change):
        if obj.calories and obj.calories != '':
            obj.calories = str(obj.calories)
        else:
            obj.calories = str(sum(((i.calories / 100) * i.amount) for i in obj.ingredients))

        super().save_model(request, obj, form, change)
