# Generated by Django 5.0.6 on 2024-11-20 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('misc', '0004_card_is_approved'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='is_approved',
            field=models.BooleanField(default=False),
        ),
    ]
