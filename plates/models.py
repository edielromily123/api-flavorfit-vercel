from django.db import models
from ingredients.models import Ingredient

class Plate(models.Model):
    is_approved = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    owner = models.ForeignKey("users.User", related_name='plate_owner', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='plates/%Y/%m/%d/', null=True, blank=True)
    name = models.TextField(unique=True)
    calories = models.CharField(max_length=10, blank=True, default=0)
    description = models.TextField()
    
    @property
    def ingredients(self):
        ingredients = []
        for i in IngredientPlate.objects.filter(plate=self):
            i.ingredient.amount = i.amount
            ingredients.append(i.ingredient)
        return ingredients

    def __str__(self):
        return f"{self.id} - {self.name}"


class IngredientPlate(models.Model):
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    plate = models.ForeignKey(Plate, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=100)
    
    class Meta:
        unique_together = ('ingredient', 'plate')