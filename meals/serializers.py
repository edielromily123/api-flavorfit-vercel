from rest_framework import serializers
from .models import Meal, MealPlate
from plates.serializers import PlateSerializer


class MealPlateSerializer(serializers.ModelSerializer):
    plate = PlateSerializer(read_only=True)
    substitutes = PlateSerializer(many=True, read_only=True)

    class Meta:
        model = MealPlate
        fields = '__all__'


class MealSerializer(serializers.ModelSerializer):
    plates = serializers.SerializerMethodField()
    diet_id = serializers.SerializerMethodField()

    class Meta:
        model = Meal
        fields = '__all__'

    def get_plates(self, meal):
        plates = MealPlate.objects.filter(meal=meal)
        if plates:
            return MealPlateSerializer(plates, many=True).data
        return [] 
    
    def get_diet_id(self, meal):
        diet = meal.diet
        
        if diet:
            return meal.diet.id
        else:
            return None
