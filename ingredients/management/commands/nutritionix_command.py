# nutritionix_command.py
import time
import requests
from django.core.management.base import BaseCommand
from ingredients.models import Ingredient
from misc.utils import write_log

class Command(BaseCommand):
    help = 'Popula o banco de dados com ingredientes da Nutritionix'

    def handle(self, *args, **kwargs):
        api_key = 'ba0226b5f85de050edc3787b6ad4c13a'
        url = f'https://api.nutritionix.com/v1_1/search/food'
        
        headers = {'x-app-id': 'SUA_APP_ID', 'x-app-key': api_key}
        response = requests.get(url, headers=headers)
        
        write_log(f'nutritionix_response__{time.time()}.json', response.json().encode())
        
        data = response.json()

        for item in data['hits']:
            name = item['fields']['item_name']
            calories = item['fields'].get('nf_calories', None)
            sodium = item['fields'].get('nf_sodium', None)
            fiber = item['fields'].get('nf_dietary_fiber', None)
            sugar = item['fields'].get('nf_sugars', None)
            proteins = item['fields'].get('nf_protein', None)

            # Traduzir o nome para pt-br
            translated_name = self.translate_to_pt_br(name)

            # Criar o ingrediente no banco
            Ingredient.objects.create(
                name=translated_name,
                calories=calories,
                sodium=sodium,
                fibers=fiber,
                sugar=sugar,
                proteins=proteins,
                is_approved=False,
                is_active=True
            )

    def translate_to_pt_br(self, text):
        # Aqui você pode usar a API de tradução, por exemplo, Google Translate ou outro serviço.
        # Exemplo simples com Google Translate:
        import googletrans
        translator = googletrans.Translator()
        translation = translator.translate(text, src='en', dest='pt')
        return translation.text
