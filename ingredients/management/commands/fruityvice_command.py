from decimal import Decimal, InvalidOperation
import time 
import requests 
from django.core.management.base import BaseCommand 
from ingredients.models import Ingredient 
from misc.utils import translate_to_pt_br, write_log, read_log
import traceback


class Command(BaseCommand):
    request_count = 0
    help = 'Popula o banco de dados com frutas usando a API Open Food Facts'

    def handle(self, *args, **kwargs):
        base_url = "https://br.openfoodfacts.org/cgi/search.pl"
        query = ""
        self.populate_fruits(base_url, query, 69)

    def populate_fruits(self, url, query, page):
        params = {
            # 'search_terms': query,
            'search_simple': 1,
            'action': 'process',
            'json': 1,
            'page': page,
            'fields': 'product_name,nutriments',
            'page_size': 100,
        }
        name = None
        try:
            data = read_log(f'open_food_facts_responses/open_food_facts_response_page_{page}.json')
            
            if not data:
                self.request_count += 1
                
                if self.request_count % 10 == 0:
                    time.sleep(600)
                
                self.stdout.write(f"Fazendo requisição")
                response = requests.get(url, params=params)
                
                if response.status_code != 200:
                    self.stdout.write(f"Erro na API Open Food Facts: {response.status_code}")
                    return

                write_log(f'open_food_facts_responses/open_food_facts_response_page_{page}.json', response.json())  # Salva o log da resposta
                
                data = response.json()
            else:
                self.stdout.write(f"Requisição já feita")
                
            products = data.get('products', [])

            for product in products:
                try:
                    name = product.get('product_name', None)
                    if not name:
                        continue  # Ignora produtos sem nome

                    nutriments = product.get('nutriments', {})
                    calories = nutriments.get('energy-kcal_100g', None)
                    sodium = nutriments.get('sodium_100g', None)
                    fiber = nutriments.get('fiber_100g', None)
                    sugar = nutriments.get('sugars_100g', None)
                    proteins = nutriments.get('proteins_100g', None)

                    try:
                        calories = Decimal(calories) if calories else None
                        sodium = Decimal(sodium) if sodium else None
                        fiber = Decimal(fiber) if fiber else None
                        sugar = Decimal(sugar) if sugar else None
                        proteins = Decimal(proteins) if proteins else None
                    except InvalidOperation as e:
                        print(f"Erro ao converter valores numéricos: {e}")
                        return

                    Ingredient.objects.get_or_create(
                        name=name,
                        defaults={
                            'calories': calories,
                            'sodium': sodium,
                            'fibers': fiber,
                            'sugar': sugar,
                            'proteins': proteins,
                            'is_approved': False,
                            'is_active': True,
                        }
                    )
                    # self.stdout.write(f"Fruta adicionada: {name}")

                except Exception as e:
                    self.stdout.write(f"Falha ao adicionar {name}: {str(e)}")
                    traceback.print_exc()

            
            self.stdout.write(f"Frutas adicionadas da página {page}")
            if page * 100 < data.get('count', 0):
                self.stdout.write(f"Requisitando página {page + 1}")
                
                self.populate_fruits(url, query, page + 1)

        except Exception as e:
            self.stdout.write(f"Erro ao processar frutas: {str(e)}")

