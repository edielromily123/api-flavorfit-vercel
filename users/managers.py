from django.contrib.auth.models import UserManager
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync, sync_to_async


class CustomUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)

    def send_notification_to_user(self, user_id, notification):
        channel_layer = get_channel_layer()
        
        print("Sending notification to user: ", f"user_{user_id}")
        async_to_sync(channel_layer.group_send)(
            f"user_{user_id}",
            notification.to_dict()
        )
    
    def send_message_to_user(self, user_id, message):
        channel_layer = get_channel_layer()
        
        if isinstance(user_id, list):
            for id in user_id:
                async_to_sync(channel_layer.group_send)(
                    f"user_{id}",
                    message.to_dict()
                )
        else:
            async_to_sync(channel_layer.group_send)(
                f"user_{user_id}",
                message.to_dict()
            )
        
    def send_notification_to_group(self, group_name, notification):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            group_name,
            notification.to_dict()
        )
        
    async def send_undelivered_notifications(self, user_id):
        from notifications.models import Notification
        from asgiref.sync import sync_to_async

        user_notifications = await sync_to_async(lambda: list(Notification.objects.filter(user_id=user_id, is_delivered=False)))()
        
        # Imprimir as notificações para depuração
        await sync_to_async(print)(user_notifications)

        channel_layer = get_channel_layer()
        
        for notification in user_notifications:
            await channel_layer.group_send(
                f"user_{user_id}",
                notification.to_dict()
            )
            
            print("Notification sent to user: ", f"user_{user_id}")
            notification.is_delivered = True
            # await sync_to_async(notification.save)()
    
    
    async def send_undelivered_messages(self, user_id):
        from chat.models import Message
        
        user_messages = await sync_to_async(lambda: list(Message.objects.filter(chat__users=user_id, status=0)))()
    
        channel_layer = get_channel_layer()

        for message in user_messages:
            await channel_layer.group_send(
                f"user_{user_id}",
                await sync_to_async(message.to_dict)()
            )
            await sync_to_async(message.mark_as_delivered)()


    def send_notification_to_all(self, notification):
        """Envia notificação para todos os usuários autenticados e não autenticados"""
        self.send_notification_to_unauthenticated_users(notification)
        
        self.send_notification_to_all_users(notification)
        

    def send_notification_to_unauthenticated_users(self, notification):
        """Envia notificação para usuários não autenticados"""
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            "unauthenticated_users",
            notification.to_dict()
        )

    def send_notification_to_all_users(self, notification):
        """Envia notificação para todos os usuários autenticados"""
        channel_layer = get_channel_layer()
        users = self.get_queryset()
        for user in users:
            async_to_sync(channel_layer.group_send)(
                f"user_{user.id}",
                notification.to_dict()
            )
