from rest_framework import serializers

from misc.utils import write_log
from plates.models import IngredientPlate
from .models import Ingredient

class IngredientSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField()
    amount = serializers.SerializerMethodField()
    
    class Meta:
        model = Ingredient
        fields = '__all__'
        
    def get_owner(self, instance):
        if "request" not in self.context:
            return False
        
        return instance.owner.id == self.context['request'].user.id
    
    def get_amount(self, instance):
        if "plate" not in self.context:
            return 100
        plate = self.context['plate']
        return IngredientPlate.objects.filter(plate=plate, ingredient=instance).first().amount