
from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import include, path
from chat.views import ChatViewSet
from diets.views import DietViewSet
from ingredients.views import IngredientViewSet
from meals.views import DailyHistoryViewSet, MealViewSet
from misc.views import CardViewSet
from plates.views import PlateViewSet
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from users.views import UserViewSet

from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings
from django.conf.urls.static import static

schema_view = get_schema_view(
    openapi.Info(
        title=f"{settings.APP_NAME} API",
        default_version='v1',
        description="Descrição da sua API",
        terms_of_service="https://www.example.com/policies/terms/",
        contact=openapi.Contact(email="contact@example.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


router = DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'meal', MealViewSet)
router.register(r'diet', DietViewSet)
router.register(r'plate', PlateViewSet)
router.register(r'ingredient', IngredientViewSet)
router.register(r'daily_historie', DailyHistoryViewSet)
router.register(r'cards', CardViewSet)
router.register(r'chat', ChatViewSet)


def connection_test_view(request):
    return HttpResponse("Tudo certo!", status=200)

def redirection(request):
    return redirect("about")

urlpatterns = [
    path('', redirection, name="default"),
    
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('misc/', include('misc.urls')),
    path('chat/', include('chat.urls')),
    path('test_connection/', connection_test_view, name='test_connection'),

    path('doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)