from django.db import models
from django.utils import timezone

class DailyHistoryManager(models.Manager):
    def getActualHistory(self, user):
        from users.models import UserDiet
        
        user_diet = UserDiet.objects.filter(user=user, active=True).first()
        
        from diets.models import MonthlyHistory
        
        today = timezone.now()
        monthly_history, _ = MonthlyHistory.objects.get_or_create(
            diet=user_diet,
            user=user,
            month=timezone.now().month,
            year=timezone.now().year,
        )
        
        daily_history, created = self.get_or_create(
            user=user, day=today.day, month=today.month, year=today.year
        )
        
        if daily_history not in monthly_history.daily_histories.all():
            monthly_history.daily_histories.add(daily_history)
        
        return daily_history, created
        