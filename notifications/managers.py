from django.db import models

class NotificationManager(models.Manager):
    
    def get_random_motivation(self):
        return super().get_queryset().filter(notification_group="motivation_message").order_by("?").first()