# Generated by Django 5.0.6 on 2024-11-21 23:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_approved', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=False)),
                ('name', models.TextField()),
                ('amount', models.DecimalField(decimal_places=2, default=100, max_digits=10)),
                ('calories', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('sodium', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('fibers', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('sugar', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('proteins', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
    ]
