from django import forms
from .models import Notification

class NotificationAdminForm(forms.ModelForm):
    redirection_params = forms.CharField(
        required=False,
        label="Parâmetros do Redirect",
        help_text="Adicione parâmetros para a URL, no formato chave=valor.",
    )
    
    class Meta:
        model = Notification
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        if self.instance.redirection_type == 'http':
            self.fields['redirection'].widget = forms.TextInput()
            self.fields['redirection_params'].widget = forms.HiddenInput()
        else:
            self.fields['redirection'].widget = forms.Select(choices=Notification.ABLE_TO_REDIRECT_ROUTS)
    
    def save(self, commit=True):
        instance = super().save(commit=False)
        
        if self.cleaned_data['redirection_params']:
            instance.redirection = f"{instance.redirection}{self.cleaned_data['redirection_params']}"
            print(instance.redirection)
        
        if commit:
            instance.save()
            
        
        return instance