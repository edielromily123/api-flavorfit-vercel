from django.urls import reverse
from rest_framework.test import APITestCase
from users.models import User, UserDiet
from diets.models import Diet

class DietViewTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="password123")
        self.diet = Diet.objects.create(name="Test Diet")
        self.user_diet = UserDiet.objects.create(user=self.user, diet=self.diet, active=True)

    def test_get_my_diet(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('users-my-diet', args=[self.user.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['diet']['name'], "Test Diet")
