from rest_framework import serializers

from FlavorFit import settings
from diets.models import DailyHistory, MonthlyHistory
from diets.serializers import DailyHistorySerializer, DietSerializer, MonthlyHistorySerializer
from notifications.models import Notification
from .models import User, UserDiet
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail

class UserSerializer(serializers.ModelSerializer):
    my_diet = serializers.SerializerMethodField()
    water_goal = serializers.SerializerMethodField()
    
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True},
            'last_login': {'read_only': True},
            'is_superuser': {'read_only': True},
            'is_staff': {'read_only': True},
            'is_active': {'required': False},
            'date_joined': {'read_only': True},
            'created_at': {'read_only': True},
            'groups': {'read_only': True},
            'user_permissions': {'read_only': True},
        }
        
    def get_my_diet(self, user):
        try:
            return UserDietSerializer(UserDiet.objects.get(user=user), context=self.context).data
        except UserDiet.DoesNotExist:
            return None
    
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        
        user = super().create(validated_data)
        
        send_mail(
            'Bem-vindo à nossa comunidade!',
            'Obrigado por se cadastrar.',
            f'{settings.APP_NAME}@gmail.com',
            [user.email],
            fail_silently=False,
        )
        
        Notification.objects.create(
            user=user,
            message='Conta criada com sucesso!',
            description=f'Sua conta foi criada com sucesso na {settings.APP_NAME}!',
            title='Conta criada com sucesso!',
            icon='0xf055d'
        )
        Notification.objects.create(
            user=user,
            message='Estamos felizes por tê-lo conosco na sua jornada de saúde!',
            description='Acompanhe sua dieta, peso e nutrição de forma fácil e prática.',
            title=f'Bem-vindo à {settings.APP_NAME}!',
            icon='0xf055d'
        )
        
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            validated_data['password'] = make_password(validated_data.get('password'))
        return super().update(instance, validated_data)
    
    def get_water_goal(self, user):
        return user.water_goal
    
    
    
class UserDietSerializer(serializers.ModelSerializer):
    diet = DietSerializer(read_only=True)
    history = serializers.SerializerMethodField()
    progress = serializers.SerializerMethodField()
    
    class Meta:
        model = UserDiet
        fields = '__all__'
        
    def get_history(self, d):
        try:
            return DailyHistorySerializer(d.actual_monthly.daily_histories, many=True).data
        except UserDiet.DoesNotExist:
            return None
    
    def get_progress(self, d):
        try:
            return MonthlyHistorySerializer(MonthlyHistory.objects.filter(user=d.user), many=True).data
        except UserDiet.DoesNotExist:
            return None