from django.contrib import admin
from .models import Meal, MealPlate

@admin.register(Meal)
class MealAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'hour', 'minute', 'day', 'description')
    search_fields = ('name', 'description')  # Para pesquisa no nome e descrição
    list_filter = ('hour', 'day')  # Filtros para facilitar a busca
    ordering = ('-hour',)

    def plates_count(self, obj):
        return obj.plates.count()
    plates_count.short_description = 'Plates Count'  # Exibe a quantidade de pratos relacionados a cada refeição
    list_display += ('plates_count',)


@admin.register(MealPlate)
class MealPlateAdmin(admin.ModelAdmin):
    list_display = ('meal', 'plate')
    search_fields = ('meal__name', 'plate__name')  # Pesquisa no nome da refeição e prato
    list_filter = ('meal', 'plate')  # Filtros para facilitar a busca por refeição e prato
    ordering = ('meal',)
