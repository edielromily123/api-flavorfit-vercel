from django.contrib import admin
from ingredients.models import Ingredient
from django.contrib import messages

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ('name', 'calories', 'sodium', 'fibers', 'sugar', 'proteins', 'carbohydrates', 'is_approved', 'is_active', 'owner')
    list_editable =('calories', 'sodium', 'fibers', 'sugar', 'proteins', 'carbohydrates', 'is_approved', 'is_active', 'owner')
    list_filter = ('name', 'calories', 'sodium', 'fibers', 'sugar', 'proteins', 'carbohydrates', 'is_approved', 'is_active', 'owner')  # Filtros laterais para campos booleanos e foreign key
    search_fields = ('name', 'owner__username')  # Campo de pesquisa por nome e usuário do owner
    readonly_fields = ('calories', 'sodium', 'fibers', 'sugar', 'proteins', 'carbohydrates')
    actions = ['delete_duplicate_ingredients']
    
    def delete_duplicate_ingredients(modeladmin, request, queryset):
        name_count = {}

        # Conta a quantidade de vezes que cada nome aparece
        for ingredient in Ingredient.objects.all():
            if ingredient.name in name_count:
                name_count[ingredient.name] += 1
            else:
                name_count[ingredient.name] = 1

        # Deleta os ingredientes com o mesmo nome, exceto o primeiro
        deleted_count = 0
        for name, count in name_count.items():
            if count > 1:
                # Deleta todos os ingredientes com esse nome, exceto o primeiro
                ingredients_to_delete = Ingredient.objects.filter(name=name)[1:]
                for ingredient in ingredients_to_delete:
                    ingredient.delete()
                    deleted_count += 1

        if deleted_count > 0:
            modeladmin.message_user(request, f'{deleted_count} ingrediente(s) duplicado(s) deletado(s).', level=messages.SUCCESS)
        else:
            modeladmin.message_user(request, 'Nenhum ingrediente duplicado encontrado.', level=messages.WARNING)

    delete_duplicate_ingredients.short_description = 'Remover ingredientes duplicados'