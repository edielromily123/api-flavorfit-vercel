cards_data = [
    {
        "name": "Defina seus objetivos",
        "description": "Estabeleça metas claras, como perda de peso, ganho de massa muscular ou manutenção do peso.",
        "redirect_link": "https://vivasimplesesaudavel.com/como-estabelecer-metas-de-saude-realistas-e-alcancaveis/",
        "image": "https://scopi.com.br/wp-content/webp-express/webp-images/uploads/2023/11/metas-e-objetivos.jpg.webp"
    },
    {
        "name": "Consuma variedade",
        "description": "Inclua diferentes grupos alimentares para garantir a ingestão de todos os nutrientes.",
        "redirect_link": "https://bvsms.saude.gov.br/bvs/publicacoes/guia_alimentar_populacao_brasileira_versao_resumida.pdf",
        "image": "https://tse4.mm.bing.net/th?id=OIG3.iqPbocFGCyzizw_te3vU&pid=ImgGn"
    },
    {
        "name": "Priorize alimentos integrais",
        "description": "Prefira alimentos minimamente processados, como grãos integrais, frutas e legumes.",
        "redirect_link": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2022/por-que-limitar-o-consumo-de-alimentos-processados-e-evitar-alimentos-ultraprocessados",
        "image": "https://www.harteinstrumentos.com.br/wp-content/uploads/elementor/thumbs/alimentos-integrais-post-qljrfetem1e5p8h16px3hactrishax3c0nze11pkeg.jpg"
    },
    {
        "name": "Controle as porções",
        "description": "Preste atenção ao tamanho das porções para evitar excessos.",
        "redirect_link": "https://portuguese.foodinsight.org/misc/seu-guia-para-tamanhos-de-porcoes/",
        "image": "https://images.squarespace-cdn.com/content/v1/54884604e4b08e455df8d6ff/1549033737214-QLEVALIDG77TGUS8DLTT/secrets-of-healthy-eating-and-portion-control-s9-three-different-portions_1333485361.jpg?format=1500w"
    },
    {
        "name": "Beba água",
        "description": "Mantenha-se hidratado; a água é essencial para o funcionamento do corpo.",
        "redirect_link": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2022/como-criancas-e-adultos-podem-aumentar-sua-hidratacao",
        "image": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2022/como-criancas-e-adultos-podem-aumentar-sua-hidratacao/como-criancas-e-adultos-podem-aumentar-sua-hidratacao_-_1280x530px.png/@@images/3554d97c-0f58-46ab-a0d1-57ac76975706.png"
    },
    {
        "name": "Planeje as refeições",
        "description": "Organize suas refeições e lanches para evitar escolhas alimentares impulsivas.",
        "redirect_link": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2018/cardapio-saudavel-como-planejar-suas-refeicoes",
        "image": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2018/cardapio-saudavel-como-planejar-suas-refeicoes/mercado.png/@@images/c0ed223c-83bc-46ec-b3b2-7d0017b17cfd.png"
    },
    {
        "name": "Inclua proteínas magras",
        "description": "Consuma fontes de proteína magra, como frango, peixe, leguminosas e tofu.",
        "redirect_link": "https://www.mundoboaforma.com.br/10-alimentos-ricos-em-proteinas-magras-e-completas/",
        "image": "https://www.mundoboaforma.com.br/wp-content/uploads/2022/12/peito-de-frango.webp"
    },
    {
        "name": "Consuma gorduras saudáveis",
        "description": "Opte por gorduras insaturadas, como abacate, nozes e azeite de oliva.",
        "redirect_link": "https://www.ecycle.com.br/o-que-e-gordura-insaturada/",
        "image": "https://images.ecycle.com.br/wp-content/uploads/2021/06/02155331/avocado-scaled.jpg.webp"
    },
    {
        "name": "Reduza açúcares e sódio",
        "description": "Limite a ingestão de açúcares adicionados e sódio para melhorar a saúde geral.",
        "redirect_link": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2017/sal-acucar-gorduras-os-riscos-do-excesso",
        "image": "https://www.gov.br/saude/pt-br/assuntos/saude-brasil/eu-quero-me-alimentar-melhor/noticias/2017/sal-acucar-gorduras-os-riscos-do-excesso/shutterstock_516863887.jpg/@@images/da7893be-0c9d-4b58-8faa-dc36cb0e2a7f.jpeg"
    },
    {
        "name": "Aumente a ingestão de fibras",
        "description": "Inclua alimentos ricos em fibras, como frutas, vegetais e grãos integrais, para melhorar a digestão.",
        "redirect_link": "https://www.tuasaude.com/alimentos-ricos-em-fibras/",
        "image": "https://fly.metroimg.com/upload/q_85,w_700/https://uploads.metroimg.com/wp-content/uploads/2024/03/07145255/GettyImages-1457889029.jpg"
    },
    {
        "name": "Evite dietas extremas",
        "description": "Fuja de dietas muito restritivas que podem ser insustentáveis a longo prazo.",
        "redirect_link": "https://ge.globo.com/eu-atleta/nutricao/reportagem/2024/09/19/c-como-emagrecer-sem-dieta-veja-20-dicas-sem-regime-restritivo.ghtml",
        "image": "https://s2-ge.glbimg.com/mY1CzHmgdqxjstoGKw_BR3LGjcE=/0x0:1254x836/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_bc8228b6673f488aa253bbcb03c80ec5/internal_photos/bs/2024/U/W/vbwiHmQH2xV4M1VFuqiw/emagrecimento.jpg"
    },
    {
        "name": "Faça lanches saudáveis",
        "description": "Opte por lanches nutritivos, como frutas, vegetais ou iogurte.",
        "redirect_link": "https://dicionariodanutricao.com.br/glossario/o-que-e-lanche-nutritivo/",
        "image": "https://2snutri.com/wp-content/uploads/2020/05/lanches-saudaveis-img.png"
    },
    {
        "name": "Preste atenção à mastigação",
        "description": "Mastigue bem os alimentos para facilitar a digestão e ajudar na saciedade.",
        "redirect_link": "https://www.unimed.coop.br/viver-bem/alimentacao/por-que-e-importante-mastigar-bem-os-alimentos",
        "image": "https://www.unimed.coop.br/documents/20182/19277836/habito-mastigacao.jpg/cb0de035-0fc0-4b0c-8f1e-14e092499b4f?t=1717009472633"
    },
    {
        "name": "Evite distrações durante as refeições",
        "description": "Coma com atenção, evitando distrações como televisão ou celular.",
        "redirect_link": "https://totalpass.com/br/blog/alimentacao/comer-com-mais-atencao/",
        "image": "https://totalpass.com/wp-content/uploads/2021/10/mindful-eating.jpg"
    },
    {
        "name": "Monitore seu progresso",
        "description": "Acompanhe suas refeições e progresso para ajustar a dieta conforme necessário.",
        "redirect_link": "https://minhasaude.proteste.org.br/acompanhamento-nutricional/",
        "image": "https://minhasaude.proteste.org.br/wp-content/webp-express/webp-images/uploads/2024/06/Image1.png.webp"
    },
    {
        "name": "Consulte um profissional",
        "description": "Considere trabalhar com um nutricionista para orientações personalizadas.",
        "redirect_link": "https://blog.nakednuts.com.br/nutricionista/",
        "image": "https://blog.nakednuts.com.br/wp-content/uploads/2022/12/nutricionista.png"
    },
    {
        "name": "Seja flexível",
        "description": "Permita-se pequenas indulgências sem culpa; a moderação é a chave.",
        "redirect_link": "https://www.minhavida.com.br/materias/materia-19008",
        "image": "https://static1.minhavida.com.br/articles/fd/57/15/ad/shutterstock-361400108-article-1.jpg"
    },
    {
        "name": "Escute seu corpo",
        "description": "Preste atenção aos sinais de fome e saciedade para comer de forma intuitiva.",
        "redirect_link": "https://alice.com.br/blog/sua-saude/alimentacao-intuitiva-sinais-fome-e-saciedade/",
        "image": "https://www.gov.br/saude/pt-br/assuntos/noticias/2023/janeiro/respeito-ao-ciclo-de-fome-e-saciedade-das-criancas-pequenas-ajuda-no-desenvolvimento/sem-titulo-1.png/@@images/1e6b5e97-a8ab-4bae-998c-2cc8c75ecbb8.png"
    },
    {
        "name": "Incorpore atividade física",
        "description": "Combine a dieta com exercícios regulares para resultados mais eficazes.",
        "redirect_link": "https://rafaelpersonaltrainer.com.br/blog/dieta-e-exercicios-perda-de-peso/",
        "image": "https://rafaelpersonaltrainer.com.br/wp-content/uploads/2023/12/ditas-da-moda-1536x864.jpeg"
    },
    {
        "name": "Priorize a qualidade do sono",
        "description": "Um bom sono contribui para a saúde geral e o controle de peso.",
        "redirect_link": "https://www.drsilencio.com.br/sono-e-controle-de-peso-o-que-voce-precisa-saber/",
        "image": "https://www.drsilencio.com.br/wp-content/uploads/2023/07/sono-tranquilo-janela-antiruido-1536x1024.jpg"
    }
]

import os
import random
import requests
from django.core.management.base import BaseCommand
from django.conf import settings
from misc.models import Card

class Command(BaseCommand):
    help = 'Adiciona cards ao banco de dados a partir de um arquivo JSON'

    def handle(self, *args, **kwargs):
        for card_data in cards_data:
            image_path = self.download_image(card_data['image'])

            # Criar o card no banco de dados
            card, created = Card.objects.get_or_create(
                name=card_data['name'],
                defaults={
                    'description': card_data['description'],
                    'redirect_link': card_data['redirect_link'],
                    'image': image_path,  # Caminho da imagem salva
                    'group': 'REGISTER_DIET_TIP',  # Ajuste conforme necessário
                    'bg_color': self.generate_light_color()
                }
            )
            if created:
                self.stdout.write(self.style.SUCCESS(f"Card '{card_data['name']}' criado com sucesso."))
            else:
                self.stdout.write(self.style.WARNING(f"Card '{card_data['name']}' já existe."))

    def generate_light_color(self):
        min_value = 128  # Limite mínimo para garantir que a cor seja clara

        r = random.randint(min_value, 255)
        g = random.randint(min_value, 255)
        b = random.randint(min_value, 255)

        return f'#{r:02x}{g:02x}{b:02x}'

    def download_image(self, url):
        try:
            response = requests.get(url)
            response.raise_for_status()  # Levanta um erro para códigos de status 4xx/5xx

            # Define o nome do arquivo e o caminho para onde será salvo
            image_name = url.split("/")[-1]
            local_path = os.path.join(settings.MEDIA_ROOT, 'misc', image_name)

            # Cria o diretório se não existir
            os.makedirs(os.path.dirname(local_path), exist_ok=True)

            # Salva a imagem no diretório local
            with open(local_path, 'wb') as img_file:
                img_file.write(response.content)

            # Retorna o caminho relativo à pasta media
            return os.path.join('misc', image_name)

        except requests.exceptions.RequestException as e:
            self.stdout.write(self.style.ERROR(f"Erro ao baixar a imagem: {e}"))
            return None
