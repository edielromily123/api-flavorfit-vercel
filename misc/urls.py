from django.urls import path
from . import views

urlpatterns = [
    path('terms/', views.terms, name='terms'),
    path('auth/forgot-password/', views.password_reset, name='password_reset'),
    path('auth/forgot-password/done/', views.password_reset_done, name='password_reset_done'),
    path('about/', views.about, name='about'),
    path('logs/', views.logs, name='logs'),
    
    path('logs/<str:file_name>/', views.read_log_file, name='read_log_file'),
    
    path('logs/delete/<str:file_name>/', views.delete_logs, name='deletar_log'),
    path('requests/record/start/', views.start_record, name='start_record'),
    path('requests/record/stop/', views.stop_record, name='stop_record'),
    path('download-apk/', views.download_apk, name='download_apk'),
]