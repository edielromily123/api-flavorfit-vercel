from rest_framework import serializers
from .models import Notification
from asgiref.sync import sync_to_async


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = [
            'id',
            'user',
            'redirection_type',
            'redirection',
            'description',
            'message',
            'title',
            'icon',
            'created_at',
            'is_read',
            'is_delivered'
        ]
        read_only_fields = ['id', 'created_at', 'is_delivered', 'is_read']

    async def to_representation(self, instance):
        """Custom representation to remove user detail from response if not needed."""
        representation = super().to_representation(instance)
        user = await sync_to_async(lambda: instance.user)()
        if user is None:
            representation.pop('user')  # Remove campo user se a notificação for para todos os usuários
        
        representation['type'] = 'send_notification'
        
        return representation
