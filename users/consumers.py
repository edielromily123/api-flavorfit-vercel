import json
import asyncio
from datetime import datetime

from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async, async_to_sync
from urllib.parse import parse_qs

from django.contrib.auth.hashers import check_password
from dateutil.parser import parse
from django.utils import timezone
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework_simplejwt.exceptions import TokenError

from misc.utils import write_log
from users.models import User
from chat.models import Chat, Message

class NotificationConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        
        self.user = None
        query_string = self.scope['query_string']
        self.access_token = str(query_string).replace("b'token=", "").replace("'", "")
        self.group_name = "unauthenticated_users"
        
        if self.access_token:
            
            user = await self.authenticate_user(self.access_token)
            if user:
                self.user = user
                user_id = await sync_to_async(lambda: self.user.id)()
                self.group_name = f"user_{user_id}"
                await self.channel_layer.group_add(self.group_name, self.channel_name)
                await self.send(json.dumps({
                    "message": f"Usuário {self.user.username} autenticado com sucesso.",
                    "notification_type": "success"
                }))
            else:
                write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - connect: Token inválido. Conectado como não autenticado.\n")
                await self.send(json.dumps({
                    "error": "Token inválido. Conectado como não autenticado.",
                    "notification_type": "erro",
                    "error_code": "invalid_token"
                }))
        
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        
        if self.user:
            await User.objects.send_undelivered_notifications(self.user.id)
            await User.objects.send_undelivered_messages(self.user.id)

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    async def receive(self, text_data):
        data = json.loads(text_data)
        command = data.get('command')
        data = data.get('data')

        if command == 'set_token':
            write_log("websocket_log.json", "[WebScocket] /ws/notifications - set token\n")
            await self.handle_set_token(data.get('token'))
        elif command == 'login':
            write_log("websocket_log.json", "[WebScocket] /ws/notifications - login\n")
            await self.handle_login(data)
        elif command == 'confirm_notification_received':
            write_log("websocket_log.json", "[WebScocket] /ws/notifications - confirm notification\n")
            await self.handle_confirm_notification_received(data)
        elif command == 'confirm_message_received':
            write_log("websocket_log.json", "[WebScocket] /ws/notifications - Confirm message received\n")
            await self.handle_confirm_message_received(data)
        elif command == 'logout':
            write_log("websocket_log.json", "[WebScocket] /ws/notifications - Logout\n")
            await self.handle_logout()
        elif command == 'send_message':
            # write_log("websocket_log.json", "[WebScocket] /ws/notifications - send message\n")
            await self.handle_send_message(data)

# --------------:> Métodos handle <:------------------ #
    async def handle_logout(self):
        if self.user:
            await self.channel_layer.group_discard(self.group_name, self.channel_name)
            self.user = None
            self.access_token = None
            self.group_name = "unauthenticated_users"
            await self.channel_layer.group_add(self.group_name, self.channel_name)
            await self.send(json.dumps({
                "message": "Logout realizado com sucesso.", 
                "notification_type": "success"
            }))
        else:
            write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - logout: Nenhum usuário autenticado para realizar logout.\n")
            await self.send(json.dumps({
                "error": "Nenhum usuário autenticado para realizar logout.",
                "notification_type": "erro",
                "error_code": "not_authenticated"
            }))
    
    async def handle_login(self, data):
        identifier = data.get('username', '')
        password = data.get('password')

        try:
            if '@' in identifier:
                user = await sync_to_async(User.objects.get)(email=identifier)
            else:
                user = await sync_to_async(User.objects.get)(username=identifier)
            
        except User.DoesNotExist:
            write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - login: Credenciais inválidas, usuário não encontrado\n")
            await self.send(json.dumps({
                "error": "Credenciais inválidas, usuário não encontrado.",
                "notification_type": "erro",
                "error_code": "invalid_credentials",
            }))
            return

        if not check_password(password, user.password):
            write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - login: Credenciais inválidas, senha inválida\n")
            await self.send(json.dumps({
                "error": "Credenciais inválidas, senha inválida.",
                "notification_type": "erro",
                "error_code": "invalid_credentials",
            }))
            return

        self.user = user
        refresh = await sync_to_async(RefreshToken.for_user)(user)
        self.access_token = str(refresh.access_token)

        await self.channel_layer.group_discard("unauthenticated_users", self.channel_name)
        self.group_name = f"user_{self.user.id}"
        print(self.group_name)
        await self.channel_layer.group_add(self.group_name, self.channel_name)

        await self.send(json.dumps({
            "message": f"Login como {self.user.username} bem-sucedido.",
            "access": str(self.access_token),
            "notification_type": "success",
            "refresh": str(refresh)
        }))
        
        await User.objects.send_undelivered_notifications(user.id)
        await User.objects.send_undelivered_messages(self.user.id)
        
        
    async def handle_confirm_notification_received(self, data):
        from notifications.models import Notification
        
        notification_id = data.get('notification_id', None)
            
        if notification_id:
            notification = await sync_to_async(Notification.objects.get)(id=notification_id)
            
            await sync_to_async(notification.mark_as_delivered)()
    
    async def handle_confirm_message_received(self, data):
        from chat.models import Message
        
        message_hash = data.get('message_hash', None)
            
        if message_hash:
            message = await sync_to_async(Message.objects.get)(message_hash=message_hash)
            
            await sync_to_async(message.mark_as_delivered)()
        else:
            write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - confirm message received: Message hash missing.\n")
            await self.send_to_user(self.user.id, {
                "notification_type": "error",
                "error": "Message hash missing.",
                "error_code": "missing_data",
            })
    
    async def handle_send_message(self, data):
        if not self.user:
            write_log("websocket_log.json", "[WebScocket] ERROR /ws/notifications - send message: Nenhum usuário autenticado para enviar mensagem.\n")
            await self.send_to_user(self.user.id, {
                "notification_type": "error",
                "error": "Nenhum usuário autenticado para enviar mensagem.",
                "error_code": "not_authenticated",
            })
            return
        
        message_hash = data.get('message_hash')
        chat_id = data.get('chat')
        
        if not message_hash or not chat_id:
            write_log("websocket_log.json", "[WebScocket] ERROR /ws/notifications - send message: Message hash or chat ID missing. {message_hash, chat_id}.\n")
            await self.send_to_user(self.user.id, {
                "notification_type": "error",
                "error": f"Message hash or chat ID missing. {message_hash, chat_id}.",
                "error_code": "missing_data",
            })
            return
        
        if chat_id == "supports_chat":
            chat, created = await sync_to_async(Chat.objects.get_or_create)(type="support_chat", users__in=[self.user])

            if created:
                await sync_to_async(chat.users.add)(self.user)
                
                if await sync_to_async(chat.users.count)() == 1:
                    try: 
                        admin = await sync_to_async(User.objects.get)(is_superuser=True) 
                    except Exception as e: 
                        admin = None
                        
                    if admin:
                        await sync_to_async(chat.users.add)(admin)
        else:
            chat = await sync_to_async((await sync_to_async(Chat.objects.filter)(id=chat_id)).first)()
            
            if chat:
                if not (await sync_to_async((await sync_to_async(chat.users.filter)(id=self.user.id)).exists)()):
                    await sync_to_async(chat.users.add)(self.user)
            elif chat and self.user.is_superuser:
                await sync_to_async(chat.users.add)(self.user)
            else:
                write_log("websocket_log.json", "[WebScocket] ERROR /ws/notifications - send message: Chat not found.\n")
                await self.send_to_user(self.user.id, {
                    "notification_type": "error",
                    "error": "Chat not found.",
                    "error_code": "chat_not_found",
                })
                return
        # users = await sync_to_async(list)(chat.users.all())
        
        timestamp = data.get('timestamp', timezone.now())
        if isinstance(timestamp, str):
            timestamp = parse(timestamp)
        if timestamp and timestamp.tzinfo is not None:
            timestamp = timezone.make_naive(timestamp)
            
        await sync_to_async(Message.objects.create)(
            content=data.get('message'),
            status=0,
            timestamp=timestamp,
            sender=self.user,
            chat=chat,
            message_hash=message_hash
        )

        other_users = await sync_to_async(list)(chat.users.exclude(id=self.user.id))

        await asyncio.gather(*[(User.objects.send_undelivered_messages(user.id)) for user in other_users])
        
        await self.send_to_user(self.user.id, {
            "notification_type": "confirm_message",
            "message_hash": message_hash,
            "message_status": 1,
        })


    async def handle_set_token(self, token):
        user = await self.authenticate_user(token)
        if user:
            self.user = user
            await self.channel_layer.group_discard("unauthenticated_users", self.channel_name)
            self.group_name = f"user_{self.user.id}"
            print(self.group_name)
            await self.channel_layer.group_add(self.group_name, self.channel_name)
            await self.send(json.dumps({"message": "Usuário autenticado."}))
            
            await User.objects.send_undelivered_notifications(user.id)
            await User.objects.send_undelivered_messages(self.user.id)
        else:
            write_log("websocket_log.json", "[WebScocket] ERRO /ws/notifications - set token: Token inválido.\n")
            await self.send(json.dumps({
                "error": "Token inválido.",
                "notification_type": "erro",
                "error_code": "invalid_token"
            }))

# --------------:> Métodos de envio <:------------------ #
    async def notify_users(self, user_id, message):
        if not isinstance(user_id, list):
            await self.channel_layer.group_send(
                f"user_{user_id}",
                message.to_dict()
            )
        else:
            for id in user_id:
                await self.channel_layer.group_send(
                    f"user_{id}",
                    message.to_dict()
                )
                
    async def send_undelivered_messages(self):
        messages = await Message.objects.filter(status=0)
        
        for message in messages:
            await self.send_to_user([u.id for u in message.chat.users], {
                "notification_type": "chat_message",
                **message.to_dict()
            })

    async def notify_group(self, group_name, notification):
        await self.channel_layer.group_send(
            group_name,
            notification.to_dict()
        )

    async def authenticate_user(self, token):
        try:
            access_token = AccessToken(token)
            user_id = access_token['user_id']
            user = await sync_to_async(User.objects.get)(id=user_id)
            return user
        except (User.DoesNotExist, KeyError, TokenError):
            return None

# ------------------> Métodos util <:------------------ #
    async def send_to_user(self, user_id, data):
        if not isinstance(user_id, list):
            await self.channel_layer.group_send(
                f"user_{user_id}",
                {
                    'type': 'send_notification',
                    **data,
                }
            )
        else:
            for id in user_id:
                await self.channel_layer.group_send(
                    f"user_{id}",
                    {
                        'type': 'send_notification',
                        **data,
                    }
                )
        
# ------------------> Métodos type <:------------------ #
    async def send_notification(self, notification):
        await self.send(text_data=json.dumps(notification))