from meals.models import Meal, MealPlate
from meals.serializers import MealSerializer
from meals.permissions import IsOwnerOrReadOnly as historyIsOwner
from diets.models import DailyHistory, MonthlyHistory, MealHistory
from diets.serializers import DailyHistorySerializer, MonthlyHistorySerializer
from misc.utils import write_log
from users.permissions import IsOwnerOrReadOnly as ownerIsOwner
from plates.models import Plate

from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny, SAFE_METHODS


class MealViewSet(viewsets.ModelViewSet):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer
    permission_classes = [IsAuthenticated, ownerIsOwner]
    
    def get_permissions(self):
        """
        Define as permissões com base na ação.
        """
        if self.action in SAFE_METHODS or self.action in ["list", "conclude_meal", "conclude_meal_by_id", "drink_water"]:
            return [AllowAny()]
        
        return super().get_permissions()
    
    @action(detail=True, methods=['post'])
    def conclude_meal_by_id(self, request, pk=None):
        meal = self.get_object()

        if not meal:
            return Response({'error': 'Object Not Found'}, status=status.HTTP_404_NOT_FOUND)

        user = request.user
        u_diet = user.get_active_user_diet()

        if not u_diet:
            return Response({
                'error': 'Nenhuma dieta ativa para este usuário!'
            }, status=status.HTTP_404_NOT_FOUND)

        daily_history, _ = DailyHistory.objects.getActualHistory(user)

        meal_history = MealHistory.objects.filter(daily_history=daily_history, meal=meal).first()
        if not meal_history:
            meal_history = MealHistory.objects.create(daily_history=daily_history, meal=meal)

        # Serializa a refeição e adiciona o campo concluded_at
        meal_data = MealSerializer(meal).data
        meal_data['concluded_at'] = meal_history.concluded_at

        return Response(meal_data, status=status.HTTP_200_OK)
    
    

    @action(detail=False, methods=['post'])
    def add_extra_meal(self, request):
        user = request.user
        data = request.data

        # Verifica se o DailyHistory para o dia já existe
        daily_history, created = DailyHistory.objects.getActualHistory(user=user)
        
        # Cria ou recupera o Meal
        meal_data = {
            'name': data['name'],
            'hour': data['hour'],
            'minute': data['minute'],
            'day': data['day'],
            'description': data['description']
        }
        meal, _ = Meal.objects.get_or_create(**meal_data)

        # Cria o MealHistory
        meal_history = MealHistory.objects.create(
            daily_history=daily_history,
            meal=meal,
            is_extra=True,
            concluded_at=data.get('concluded_at', timezone.now())
        )

        # Associa os plates
        plates_data = data.get('plates', [])
        for plate_data in plates_data:
            plate = Plate.objects.get(id=plate_data['plate']['id'])
            MealPlate.objects.create(meal=meal, plate=plate)

        return Response({'message': 'Refeição extra adicionada com sucesso!'})
        
        
    @action(detail=False, methods=['post'])
    def drink_water(self, request, pk=None):
        user = request.user
        water = request.data.get("water")
        
        write_log("log.json", f"Water: {water}, User: {user}")
        
        if not water or float(water) <= 0:
            return Response({
                'water': ["Este campo é obrigatório e deve ser um valor numérico positivo"]
            }, status=status.HTTP_400_BAD_REQUEST)

        daily_history, _ = DailyHistory.objects.getActualHistory(user)
        daily_history.water_consumed = water if water <= daily_history.user.water_goal else daily_history.user.water_goal
        daily_history.save()

        return Response({
            'status': 'Agua adicionada com sucecsso!',
            'daily_history': DailyHistorySerializer(daily_history).data
        }, status=status.HTTP_200_OK)
    

class DailyHistoryViewSet(viewsets.ModelViewSet):
    queryset = DailyHistory.objects.all()
    serializer_class = DailyHistorySerializer
    permission_classes = [IsAuthenticated, historyIsOwner]

    @action(detail=False, methods=['get'])
    def get_actually(self, request):
        user = request.user
        today = timezone.now()

        daily_history, created = DailyHistory.objects.get_or_create(
            user=user, day=today.day, month=today.month, year=today.year
        )

        return Response(DailyHistorySerializer(daily_history).data, status=status.HTTP_200_OK)
    
                       
class MonthlyHistoryViewSet(viewsets.ModelViewSet):
    queryset = MonthlyHistory.objects.all()
    serializer_class = MonthlyHistorySerializer
    permission_classes = [IsAuthenticated, historyIsOwner]

    @action(detail=False, methods=['get'])
    def get_actually(self, request):
        user = request.user
        today = timezone.now()

        monthly_history, created = MonthlyHistory.objects.get_or_create(
            user=user, month=today.month, year=today.year
        )
        
        daily_history, created = DailyHistory.objects.getActualHistory(user)
        
        if daily_history not in monthly_history.daily_histories.all():
            monthly_history.daily_histories.add(daily_history)
            monthly_history.save()

        return Response(MonthlyHistorySerializer(monthly_history).data, status=status.HTTP_200_OK)
