from django import forms
from .models import Card

class CardAdminForm(forms.ModelForm):
    group = forms.MultipleChoiceField(
        choices=Card.GROUPS,
        widget=forms.CheckboxSelectMultiple,
        label="Grupos",
        help_text="Selecione um ou mais grupos para o card."
    )

    class Meta:
        model = Card
        fields = '__all__'

    def save(self, commit=True):
        instance = super().save(commit=False)
        
        selected_groups = self.cleaned_data['group']
        instance.group = ','.join(selected_groups)
        
        if commit:
            instance.save()
        
        return instance


class UpdateFieldForm(forms.Form):
    FIELD_CHOICES = [
        ('bg_color', 'Cor de fundo'),
        ('group', 'Grupo'),
    ]
    
    selected_field = forms.ChoiceField(choices=FIELD_CHOICES, label='Campo a ser alterado')
    new_value = forms.CharField(label='Novo valor', max_length=100)
    selected_ids = forms.ModelMultipleChoiceField(
        label='Cards',
        queryset=Card.objects.all(),
        widget=forms.CheckboxSelectMultiple()
    )
    
