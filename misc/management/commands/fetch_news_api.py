import requests
from io import BytesIO

from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from misc.models import Card

GNEWS_API_KEY = '4c3c4978c86e74443d6a1fcf2997be3c'

class Command(BaseCommand):
    help = 'Busca notícias em português e preenche o banco de dados com Cards.'

    def fetch_news_articles(self, query="nutrição"):
        gnews_api_url = f'https://gnews.io/api/v4/search?q={query}&lang=pt&token={GNEWS_API_KEY}'
        news_response = requests.get(gnews_api_url)
        
        if news_response.status_code == 200:
            return news_response.json().get('articles', [])
        else:
            return []

    def save_image(self, url):
        try:
            response = requests.get(url, stream=True)
            response.raise_for_status()

            img_temp = NamedTemporaryFile()
            for chunk in response.iter_content(1024):
                img_temp.write(chunk)
            
            img_temp.flush()

            return File(img_temp)
        
        except requests.exceptions.RequestException as e:
            self.stdout.write(self.style.ERROR(f"Erro ao baixar a imagem: {e}"))
            return None

    def save_card_to_db(self, article, group):
        if Card.objects.filter(name=article['title']).exists():
            return
        
        card = Card(
            is_active=False,
            group=group,  
            name=article['title'],
            description=article['description'] or "Descrição não disponível.",
            bg_color="FFFF5F5F5",  
            redirect_link=article['url'],
        )
        
        if article.get('image'):
            image_file = self.save_image(article['image'])
            if image_file:
                card.image.save(f"{card.name[:50]}.jpg", image_file, save=False)
        
        card.save()
        self.stdout.write(self.style.SUCCESS(f"Card '{card.name}' salvo com sucesso!"))

    def handle(self, *args, **kwargs):
        prompts = [
            "Receitas saudáveis",
            "dicas de nutrição",
            "Nutrição esportiva",
            "longevidade e bem-estar",
            "Alimentos funcionais",
            "Guias de nutrição",
            "Nutrição infantil",
            "hábitos alimentares",
            "Superalimentos",
            "Tendências em alimentação",
            "estilo de vida saudável",
            "suplementação alimentar"
        ]
        group = "TEMP"
        for i in prompts:
            prompt = i
            try:
                # prompt = input("Digite o prompt para pesquisa: ")
                # group = (input("\nDigite o grupo desses cards: ")).upper()
                
                if len(group) == 0:
                    group = 'HOME'
                    
                if len(prompt) > 0:
                    articles = self.fetch_news_articles(prompt)
                else:
                    articles = self.fetch_news_articles()
                    
                
                for article in articles:
                    self.save_card_to_db(article, group)
            except KeyboardInterrupt as e:
                pass
