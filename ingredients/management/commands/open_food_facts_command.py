# open_food_facts_command.py
import time
import requests
from django.core.management.base import BaseCommand
from ingredients.models import Ingredient
from misc.utils import translate_to_pt_br, write_log

class Command(BaseCommand):
    help = 'Popula o banco de dados com ingredientes do Open Food Facts'

    def handle(self, *args, **kwargs):
        url = 'https://world.openfoodfacts.org/api/v0/product/12345.json'  # Exemplo de um ID de produto
        response = requests.get(url)
        write_log(f'nutritionix_response__{time.time()}.json', response.json().encode())
        
        data = response.json()

        for product in data['product']:
            name = translate_to_pt_br(product.get('product_name', 'Desconhecido'))
            calories = product.get('nutriments', {}).get('energy-kcal_100g', None)
            sodium = product.get('nutriments', {}).get('sodium', None)
            fiber = product.get('nutriments', {}).get('fiber', None)
            sugar = product.get('nutriments', {}).get('sugars', None)
            proteins = product.get('nutriments', {}).get('proteins', None)

            # Traduzir o nome para pt-br
            translated_name = self.translate_to_pt_br(name)

            # Criar o ingrediente no banco
            Ingredient.objects.create(
                name=translated_name,
                calories=calories,
                sodium=sodium,
                fibers=fiber,
                sugar=sugar,
                proteins=proteins,
                is_approved=False,
                is_active=True
            )